Installation
============

You can install `pydockerauth` in the normal way:

    ``$ python setup.py install --user``

or

    ``$ python setup.py install --prefix=/usr/local``

or similar.  If you use the ``--user`` option, `pydockerauth` will be
installed in ``$HOME/.local/{bin,lib}`` or thereabouts.

You can build the `pydockerauth` documentation via

    ``$ python setup.py build_sphinx``

The documentation will be available in `doc/build/html`.
