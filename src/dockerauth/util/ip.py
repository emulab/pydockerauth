import socket
import struct
import traceback

class InvalidAddress(Exception):
    pass

class InvalidMaskedAddress(Exception):
    pass

class IPAddress(object):
    def __init__(self,string,addrbytes,addrint):
        """
        Create an IPAddress object.  You should not call this method;
        instead, call :func:`IPAddress.parse`, or directly call
        :func:`IPv4Address.a` or :func:`IPv6Address.a`.
        """
        self.string = string
        """A human-readable address string"""
        self.bytes = addrbytes
        """A network-order byte string"""
        self.int = addrint
        """A host-ordered integer"""

    def bytelen(self):
        """:returns: The length of the address in bytes."""
        return len(self.bytes)

    @staticmethod
    def parse(name_or_addr,resolve=True):
        """
        Convert the given hostname or address into a
        :class:`IPv4Address` or :class:`IPv6Address`.

        :param name_or_addr: hostname or address string
        :param resolve: ``True`` if hostname resolution should be tried if :func:`socket.inet_pton` fails; ``False`` if not.
        """
        try:
            return IPv6Address.a(name_or_addr)
        except:
            #traceback.print_exc()
            pass
        try:
            return IPv4Address.a(name_or_addr)
        except:
            #traceback.print_exc()
            pass
        if not resolve:
            raise InvalidAddress("could not parse '%s' to an IPv4/6 address"
                                 % (name_or_addr))
        try:
            return IPAddress.parse(socket.gethostbyname(name_or_addr),
                                   resolve=False)
        except:
            #traceback.print_exc()
            raise InvalidAddress("could not parse or resolve '%s' to an"
                                 " IPv4/6 address" % (name_or_addr))
    pass

class IPv4Address(IPAddress):
    @staticmethod
    def a(addr):
        """
        Convert an IPv4 address string to a :class:`IPv4Address` object.

        :param addr: an IPv4 address string
        :returns: an :class:`IPv4Address` object, or throws an exception.
        """
        addrs = socket.inet_pton(socket.AF_INET,addr)
        (addrint,) = struct.unpack("!I",addrs)
        return IPv4Address(addr,addrs,addrint)

    @staticmethod
    def n(bytes):
        """
        Convert a network byte order IPv4 address to a :class:`IPv4Address` object.

        :param bytes: a network byte order IPv4 address.
        :returns: an :class:`IPv4Address` object, or throws an exception.
        """
        string = socket.inet_ntop(socket.AF_INET,bytes)
        (addrint,) = struct.unpack("!I",bytes)
        return IPv4Address(string,bytes,addrint)

    def __repr__(self):
        return "IPv4Address(%s)" % (self.string)

class IPv6Address(IPAddress):
    @staticmethod
    def a(addr):
        """
        Convert an IPv6 address string to a :class:`IPv6Address` object.

        :param addr: an IPv6 address string
        :returns: an :class:`IPv6Address` object, or throws an exception.
        """
        addrs = socket.inet_pton(socket.AF_INET6,addr)
        (_hi,_lo) = struct.unpack("!QQ",addrs)
        addrint = (_hi << 64) | _lo
        return IPv6Address(addr,addrs,addrint)

    @staticmethod
    def n(bytes):
        """
        Convert a network byte order IPv6 address to a :class:`IPv6Address` object.

        :param bytes: a network byte order IPv6 address.
        :returns: an :class:`IPv6Address` object, or throws an exception.
        """
        string = socket.inet_ntop(socket.AF_INET6,bytes)
        (_hi,_lo) = struct.unpack("!QQ",bytes)
        addrint = (_hi << 64) | _lo
        return IPv6Address(string,bytes,addrint)

    def __repr__(self):
        return "IPv6Address(%s)" % (self.string)

class MaskedAddress(object):
    def __init__(self,addr,maskaddr,maskbits):
        """
        Create a :class:`MaskedAddress` object.  You should not call this
        directly unless you know what you're doing; instead, call
        :func:`MaskedAddress.parse` to parse one from a string.

        :param addr: an :class:`IPAddress` object
        :param maskaddr: an :class:`IPAddress` object representing a mask
        :param maskbits: an int representing the number of bits in the mask

        .. note:: maskaddr and maskbits are not validated for agreement in this constructor!  Currently, :func:`parse` performs this validation.
        """
        self.addr = addr
        """An :class:`IPAddress` object representing the source address."""
        self.mask = maskaddr
        """An :class:`IPAddress` object representing the mask address."""
        self.maskbits = maskbits
        """An int representing the number of bits in the mask address."""

    @staticmethod
    def parse(src):
        """
        Extract a :class:`MaskedAddress` object from the given src.  src may be
        an IP address or hostname, optionally followed by a mask IP
        address, or a int specifying the number of bits in the mask.
        First we look for a mask; if that is not present, the mask is
        the full address length (i.e., a host mask).  Second, we try to
        parse an ipv6, ipv4 address, or hostname.  It is strange to
        think of masking a hostname, but no need to special-case it.
        
        :param src: a string containing an <addr_or_hostname>[/<mask_or_maskbits>].
        :return: a :class:`MaskedAddress` object
        """
        idx = src.find('/')
        if idx == 0:
            raise InvalidMaskedAddress("invalid src '%s'" % (src,))
        name_or_addr = src
        maskstr = None
        (masks,maskbits) = (None,None)
        if idx > 0:
            name_or_addr = src[:idx]
            maskstr = src[(idx+1):]
        addr = IPAddress.parse(name_or_addr)
        if maskstr is None:
            maskstr = str(addr.bytelen() * 8)
        #
        # Parse the mask string.  First see if it's just a number; if
        # so, create the mask packed addr, etc.  Otherwise, parse an
        # address, and count the top bits.
        #
        try:
            maskbits = int(maskstr)
            if maskbits < 1 or maskbits > (addr.bytelen() * 8):
                raise InvalidMaskedAddress("invalid mask %d" % (maskbits,))
            masktop = 1 << (addr.bytelen() * 8 - 1)
            maskint = masktop
            for i in range(0,maskbits-1):
                maskint = masktop | (maskint >> 1)
            if addr.bytelen() == 16:
                (_hi,_lo) = ((maskint >> 64) & 0xffffffffffffffff,
                             maskint & 0xffffffffffffffff)
                masks = struct.pack("!QQ",_hi,_lo)
                maskaddr = IPv6Address.n(masks)
            else:
                masks = struct.pack("!I",maskint)
                maskaddr = IPv4Address.n(masks)
        except:
            #traceback.print_exc()
            maskaddr = IPAddress.parse(maskstr,resolve=False)
            maskbits = 0
            tmp = maskaddr.int
            for i in range(0,len(masks) * 8 - 1):
                if maskbits > 0 and (tmp & 0x1) == 0:
                    raise InvalidMaskedAddress("invalid mask address '%s'"
                                               % (maskstr))
                maskbits += tmp & 0x1
                tmp = tmp >> 1
            pass
        return MaskedAddress(addr,maskaddr,maskbits)

    def __repr__(self):
        return "MaskedAddress(%r,%r,bits=%d)" \
          % (repr(self.addr),repr(self.mask),self.maskbits)
