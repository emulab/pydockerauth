
import time

def isiterable(x):
    """
    :param x: any Python value.
    :returns: True if ``x`` is iterable; False otherwise.
    """
    try:
        iter(x)
        return True
    except:
        return False
    pass

def utcoffset():
    """
    Return the offset from local time to UTC time, in seconds.
    """
    if time.daylight:
        return time.altzone
    else:
        return time.timezone

def parse_bool_or_int(str):
    if str is None:
        return
    str = str.lstrip(' ').rstrip(' ')
    if str in ['True','TRUE','true','T','t','yes','YES','Yes','Y','y']:
        return True
    elif str in ['False','FALSE','false','F','f','no','NO','No','N','n']:
        return False
    else:
        return int(str)
    pass

def str2bytes(maybe_str):
    if not isinstance(maybe_str,bytes) and isinstance(maybe_str,str):
        maybe_str = maybe_str.encode()
    return maybe_str

def bytes2str(maybe_bytes):
    if isinstance(maybe_bytes,bytes) and not isinstance(maybe_bytes,str):
        maybe_bytes = maybe_bytes.decode()
    return maybe_bytes
