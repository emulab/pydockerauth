import threading

class RWLock(object):
    """
    A simple read-write lock class with a standard API.
    """

    def __init__(self,condlock=None):
        """
        Create an :class:`RWLock` object.
        
        :param condlock: a :class:`threading.Lock` object that is passed to a :class:`threading.Condition` constructor.  If unset, :class:`threading.Condition` will create one.  This optional argument may be useful to share one lock amongst multiple :class:`threading.Condition` objects.
        """
        self.cond = threading.Condition(condlock)
        self.readers = {}
        self.writer = None

    def acquire(self,timeout=None):
        """
        Acquire a read lock.  Multiple threads can acquire read locks
        simultaneously; but no threads can obtain a write lock until all
        readers have called :func:`release`.  Throws a
        :class:`exceptions.RuntimeError` if the calling thread already
        owns the lock.
        
        :param timeout: if None, wait forever to acquire.  If timeout is not None, wait a max of timeout seconds, and raise a RuntimeError if the timeout is reached prior to obtaining the lock.
        """
        self.cond.acquire()
        while self.writer:
            self.cond.wait(timeout)
            break
        tid = threading.currentThread().ident
        if tid in self.readers:
            self.cond.release()
            raise RuntimeError("thread %r already a reader" % (repr(tid)))
        self.readers[tid] = True
        self.cond.release()

    def release(self):
        """
        Releases the calling thread's read or write lock.  Throws a
        :class:`exceptions.RuntimeError` if the calling thread does not
        own the lock.
        """
        self.cond.acquire()
        tid = threading.currentThread().ident
        if self.writer == tid:
            self.writer = None
            self.cond.notify()
            self.cond.release()
        elif tid in self.readers:
            del self.readers[tid]
            if len(self.readers) == 0:
                self.cond.notify()
            self.cond.release()
        else:
            self.cond.release()
            raise RuntimeError("thread %r not a reader nor writer" % (repr(tid)))

    def acquire_write(self,timeout=None):
        """
        Acquire a write lock, once any current readers or writers have
        finished).  Throws a :class:`exceptions.RuntimeError` if the
        calling thread already owns the lock.

        :param timeout: if None, wait forever to acquire.  If timeout is not None, wait a max of timeout seconds, and raise a :class:`exceptions.RuntimeError` if the timeout is reached prior to obtaining the lock.
        """
        self.cond.acquire()
        while len(self.readers) > 0 or self.writer:
            self.cond.wait(timeout)
            break
        self.writer = threading.currentThread().ident
        self.cond.release()
        pass

    pass
