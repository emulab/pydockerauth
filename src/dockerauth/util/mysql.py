import six
import sys
import os
import time
import traceback
import threading
from dockerauth.log import LOG
from dockerauth.util.lock import RWLock

class ThreadedMysqlWrapper(object):
    """
    A simple, connection-per-thread, thread-safe wrapper around either
    ``MySQLdb`` (Python 2 only), or ``pymysql`` (Python 2 or 3).
    """
    def __init__(self,connargs,dbmodule=None,fakeuserbase=None):
        """
        Create a :class:`ThreadedMysqlWrapper` object.

        :param connargs: a dict of parameters to ``MySQLdb.connect`` or ``pymysql.connect``.
        :param dbmodule: specify ``MySQLdb`` or ``pymysql`` to force.
        """
        self.connargs = connargs
        self.dbmodule = dbmodule
        if "user" in self.connargs:
            self.fakeuserbase = None
        else:
            self.fakeuserbase = fakeuserbase
        if not self.dbmodule:
            try:
                import MySQLdb
                self.dbmodule = MySQLdb
                LOG.info("EmulabAuth using MySQLdb")
            except:
                try:
                    import pymysql
                    self.dbmodule = pymysql
                    LOG.info("EmulabAuth using pymysql")
                except:
                    raise RuntimeError("you need either MySQLdb or pymysql;"
                                       " pymysql is the only option for python3")
        # One connection per thread tid.
        self.dbconns = {}
        self.dbconnlock = RWLock()

    def connect(self,tries=4,reconnect=False,tryinterval=1):
        """
        Get a connection to the Mysql database for the calling thread.

        :param tries: The number of reconnection tries
        :param reconnect: Set ``True`` to force a reconnect, even if there is already a connection for this thread.
        :param tryinterval: Sleep time between reconnection retries.
        """
        tid = threading.currentThread().ident
        dbconn = None
        if not reconnect:
            self.dbconnlock.acquire()
            if tid in self.dbconns:
                dbconn = self.dbconns[tid]
            self.dbconnlock.release()
            LOG.debug("reusing existing connection for tid %s (reconnect=%s)"
                      % (str(tid),str(reconnect)))
            pass
        if not dbconn:
            ctries = tries
            ei = None
            while ctries:
                try:
                    ei = None
                    cargs = self.connargs
                    if not "user" in cargs and self.fakeuserbase:
                        cargs["user"] = "%s:%d:%d" % (
                            self.fakeuserbase,os.getpid(),tid)
                    dbconn = self.dbmodule.connect(**cargs)
                    # XXX we need this for mysql 5.5 and above to avoid metadata
                    # lock hangs, even when using MyISAM tables.
                    dbconn.autocommit(True)
                    break
                except:
                    ei = sys.exc_info()
                    ctries -= 1
                    LOG.exception(ei[1])
                    time.sleep(tryinterval)
                pass
            if ei is not None:
                six.reraise(*ei)
            LOG.debug("new connection for tid %s (reconnect=%s)"
                      % (str(tid),str(reconnect)))
            self.dbconnlock.acquire_write()
            self.dbconns[tid] = dbconn
            self.dbconnlock.release()
        return dbconn

    def unsafe_query(self,pat,sub):
        """
        Query the database without any failure handling (a stale
        connection will result in an exception, for instance).

        :param pat: A query pattern string with format characters.
        :param sub: a tuple with objects to be substituted (and quoted!) according to the format characters.
        :returns: a tuple of returned rows, where each row is itself a tuple of the fields selected by the query.
        """
        dbconn = self.connect()
        cursor = dbconn.cursor()
        cursor.execute(pat,sub)
        ret = cursor.fetchall()
        cursor.close()
        if ret is None:
            return ()
        return ret

    def query(self,pat,sub,tries=1,conntries=4,
              tryinterval=2,conntryinterval=1):
        """
        Query the database, but with retries to avoid transient failures
        (i.e. caused by a stale connection).

        :param pat: A query pattern string with format characters.
        :param sub: a tuple with objects to be substituted (and quoted!) according to the format characters.
        :param tries: The number of failed query retries (a reconnection attempt does not count as a failed query).
        :param conntries: The number of reconnection tries.
        :param tryinterval: Sleep time between query retries.
        :param conntryinterval: Sleep time between reconnection retries.

        :returns: a tuple of returned rows, where each row is itself a tuple of the fields selected by the query.
        """
        dbconn = self.connect(tries=conntries,tryinterval=conntryinterval)
        qtries = tries
        while qtries:
            LOG.debug("running '%s' %s (%d tries remaining)"
                      % (pat,str(sub),qtries))
            try:
                qtries -= 1
                cursor = dbconn.cursor()
                cursor.execute(pat,sub)
                ret = cursor.fetchall()
                cursor.close()
                if ret is None:
                    return ()
                return ret
            except:
                LOG.debug("query failure:\n" + traceback.format_exc())
                ei = sys.exc_info()
                try:
                    # MySQLdb defaults reconnect to False; pymysql
                    # defaults to True.  So force False, so we only have
                    # to have one reconnect logic here.
                    dbconn.ping(reconnect=False)
                except:
                    if conntries > 0:
                        qtries += 1
                        LOG.debug("reconnecting...")
                        dbconn = self.connect(tries=conntries,reconnect=True,
                                              tryinterval=conntryinterval)
                        conntries -= 1
                        continue
                    elif qtries > 0:
                        LOG.exception(ei[1])
                        time.sleep(tryinterval)
                        LOG.debug("retrying failed query '%s' %s"
                                  " -- %d tries remaining"
                                  % (pat,str(sub),qtries))
                        continue
                    else:
                        six.reraise(*ei)
                else:
                    if qtries > 0:
                        LOG.exception(ei[1])
                        time.sleep(tryinterval)
                        LOG.debug("retrying failed query '%s' %s"
                                  " -- %d tries remaining"
                                  % (pat,str(sub),qtries))
                        continue
                    else:
                        six.reraise(*ei)
        LOG.error("BUG")
        pass
