
import os
import json
from dockerauth.log import LOG
import os

DEFAULT_CONFIGFILE_PATHS = [ "/etc/dockerauth/config.json" ]
if os.getenv("HOME"):
    DEFAULT_CONFIGFILE_PATHS.insert(
        0,"%s/.dockerauth/config.json" % (os.getenv("HOME")))
if os.getenv("DOCKERAUTH_CONFIGFILE"):
    DEFAULT_CONFIGFILE_PATHS = [ os.getenv("DOCKERAUTH_CONFIGFILE") ]

def find_configfile():
    for fp in DEFAULT_CONFIGFILE_PATHS:
        if os.path.exists(fp) and os.access(fp,os.R_OK):
            return fp
    return None

def add_default_configfile(fp):
    global DEFAULT_CONFIGFILE_PATHS
    if not fp in DEFAULT_CONFIGFILE_PATHS:
        DEFAULT_CONFIGFILE_PATHS.insert(0,fp)
    return None

CONFIG_SECTIONS = {}
CONFIG_SECTION_ORDER = []
CONFIG_AUTHN_SECTIONS = []
CONFIG_AUTHZ_SECTIONS = []

def get_known_authn_sections():
    return CONFIG_AUTHN_SECTIONS

def get_known_authz_sections():
    return CONFIG_AUTHZ_SECTIONS

def global_config_section(cls):
    """
    A simple decorator that "registers" classes as top-level ConfigSections
    for our Config object.
    """
    global CONFIG_SECTIONS
    csname = cls.name
    if csname in CONFIG_SECTIONS:
        raise RuntimeError("section '%s' already globally registered!" % (csname))
    CONFIG_SECTIONS[csname] = cls
    CONFIG_SECTION_ORDER.append(csname)
    if cls.authnclass:
        CONFIG_AUTHN_SECTIONS.append(csname)
    if cls.authzclass:
        CONFIG_AUTHZ_SECTIONS.append(csname)
    return cls

class ConfigError(Exception):
    pass

class ConfigSection(object):
    """
    A ConfigSection processes a chunk of the json config file, and is
    "applied" to an AuthServer instance, once parsed and validated.
    Usually, a ConfigSection apply() invocation would return either an
    Authn instance, an Authz instance, or a 2-tuple of (Authn,Authz)
    instances (in the case that one backend provides both authn and
    authz.  apply() may also return None if it does not provide an
    Authn/Authz backend; perhaps it configures a customized AuthServer.
    """
    name = None
    """The json key for this section"""
    reconfig = False
    """True if this section supports online reconfiguration; False otherwise"""
    authnclass = None
    """An Authn class that should be automatically instantiated in apply()"""
    authzclass = None
    """An Authz class that should be automatically instantiated in apply()"""

    def __init__(self,configdir,jsonblob={}):
        self.configdir = configdir
        self._json = jsonblob
        self._hash = hash(json.dumps(jsonblob,
                                     sort_keys=True,separators=(',',':')))
        return

    def __eq__(self,other):
        """
        :returns: True if :param self: is identical to :param other: ; False otherwise.
        """
        return type(self) == type(other) and self._hash == other._hash

    def __ne__(self,other):
        """
        :returns: True if self differs from :param other: ; False otherwise.
        """
        return not self.__eq__(other)

    def apply(self,server,oldsection=None,oldobjects=()):
        """
        Applies this section to the server, either by return Authn, Authz,
        (Authn,Authz), or None (and customizing the server some other way).

        If self.reconfig is True, this method may also be called for
        online reconfiguration of a running (locked) server.  If you
        want to save the old :param oldobjects:, then instead of
        returning a new Authn or Authz or (Authn,Authz), return the
        (possibly modified) oldobjects.
        """
        (authn,authz) = (None,None)
        if self.authnclass is not None:
            authn = self.authnclass(self,server)
        if self.authzclass is not None and self.authzclass != self.authnclass:
            authz = self.authzclass(self,server)
        if authn and authz:
            return (authn,authz)
        elif authn:
            return authn
        elif authz:
            return authz
        else:
            return None

    def dump(self,full=False,indent=0):
        return repr(self)

    pass

class Config(object):
    """
    An object containing any parsed `ConfigSection` objects, as well as
    Authn/Authz instance objects that resulted from the parse.

    Each ConfigSection must stand by itself, depending on no other
    ConfigSections.  This restriction is to allow online reconfiguration
    of an existing server, given that we allow some sections to prevent
    their reconfiguration.  For instance, suppose the user edits the
    config file for a running server, and changes the server host/port
    AND adds a new static user.  We want to pick up the new static user,
    but we will not restart the server on a new port.  Thus, in that
    case, we need to be able to graft the old, unmodifiable sections of
    the old config with the modified (or modifiable) sections of the new
    config.  Thus each config section must be independent.
    """
    def __init__(self,configfile=find_configfile(),sections={},order=[]):
        """
        Parses :param configfile:, either according to :param sections:
        and :param order: (although if sections is specified and order
        is not, order will be automatically created as sections.keys()),
        or to the global CONFIG_SECTIONS and CONFIG_SECTION_ORDER vars
        that are updated by the `global_config_section` decorator.  The
        fields resulting from the parsed config are set (by section
        name) in this object.  Any state (Authn, Authz, other object)
        returned by `section.apply()` is stored as generic state in the
        server as well.
        """
        self.configfile = configfile
        st = os.stat(self.configfile)
        self.timestamp = st.st_mtime
        fd = open(self.configfile,'r')
        contents = fd.read()
        fd.close()
        self.configdir = os.path.dirname(self.configfile)
        jsonblob = json.loads(contents)
        if sections and len(sections) > 0:
            self.sections = sections
            self.order = order or list(self.sections.keys())
        else:
            self.sections = CONFIG_SECTIONS
            self.order = CONFIG_SECTION_ORDER
        
        for section in self.order:
            blob = {}
            if section in jsonblob:
                blob = jsonblob[section]
            if not section in self.sections:
                raise ConfigError("section '%s' specified in ordering, but no class"
                                  " specified to process it!" % (section))
            ns = self.sections[section](self.configdir,blob)
            setattr(self,section,ns)
            LOG.debug("config section: " + ns.dump())
        pass

    def is_stale(self):
        """Check if the config file has been updated since last parse."""
        st = os.stat(self.configfile)
        if self.timestamp < st.st_mtime:
            return st.st_mtime
        return False

    def reload(self):
        """Reload the config file.  :returns: a new Config instance."""
        return Config(self.configfile,sections=self.sections,order=self.order)

    def apply(self,server,newconfig=None):
        """
        Applies this Config to the :param server:.
        """
        if newconfig is not None:
            self.timestamp = newconfig.timestamp
        for section in self.order:
            sc = getattr(self,section)
            if not sc:
                # This is the first time applying this config file to the server;
                # handle differently.
                raise ConfigError("missing section '%s'" % (str(section)))
            isdiff = False
            newsection = None
            if newconfig:
                newsection = getattr(newconfig,section)
                isdiff = not (sc == newsection)
            if isdiff:
                if getattr(sc,'reconfig') == True:
                    LOG.info("reconfiguring server with updated section '%s'"
                             % (section,))
                    oso = server.get_section_objects(section)
                    ret = newsection.apply(server,oldsection=sc,oldobjects=oso)
                    LOG.info("saving newsection '%s' configuration after update"
                             % (section,))
                    setattr(self,section,newsection)
                    server.process_section_objects(section,ret)
                else:
                    LOG.warning("modified section '%s' in %s not reconfigurable;"
                                " ignoring!" % (section,self.configfile))
            elif not newconfig:
                LOG.debug("configuring server with section '%s'" % (section,))
                ret = sc.apply(server)
                server.process_section_objects(section,ret)
            pass
        return 0

    def dump(self,full=False):
        retval = ""
        for section in self.order:
            if hasattr(self,section):
                st = getattr(self,section).dump(indent=4)
                retval += "  Section '%s':\n    %s\n" % (section,st)
        return retval
    
    pass
