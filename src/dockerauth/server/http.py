import base64
from cryptography.hazmat.backends import default_backend
from cryptography.x509 import load_pem_x509_certificate
from dockerauth.log import LOG
from dockerauth.docker.scope import Scope
from dockerauth.util import isiterable
from dockerauth.util import bytes2str,str2bytes

class InvalidRequest(Exception):
    pass

class BaseRequest(object):
    """
    A simple HTTP request adapter class that provides uniform access to
    various bits in the request which
    :class:`dockerauth.server.auth.AuthServer` requires.  Functions as
    an adapter between the web framework in use (i.e., Flask, Twisted)
    and :class:`dockerauth.server.auth.AuthServer` request handling.

    Users are free to override key
    :class:`dockerauth.server.auth.AuthServer` methods like
    :func:`dockerauth.server.auth.AuthServer.authenticate` and
    :func:`dockerauth.server.auth.AuthServer.authorize` that use their
    own Request subclasses, or completely different classes (such as
    their framework's class).
    """
    def __init__(self,method,uri,client_ip,
                 args=dict({}),headers=dict({}),environ=dict({}),
                 framework_request_obj=None):
        """
        :param method: the HTTP method.
        :param uri: the original, full URI.
        :param client_ip: the client's IP as a string.
        :param args: a py:type:`dict` of key/value pairs, where the values might be lists if the parameter is multi-valued.
        :param headers: a py:type:`dict` of HTTP headers.
        :param environ: a py:type:`dict` of the server environment.
        :param framework_request_obj: the framework's original Request object.
        """
        self.method = method
        self.uri = uri
        self.client_ip = client_ip
        self.args = args
        self.headers = headers
        self.environ = environ
        self._framework_request_obj = framework_request_obj

    def __repr__(self):
        return "BaseRequest(%s,%s,%s args=%d,headers=%d,env=%d)" \
          % (self.method,self.client_ip,self.uri,
             len(self.args),len(self.headers),len(self.environ))

class DockerRequestMixin(object):
    """
    A request mixin that extracts the Docker registry API arguments and
    presents them as properties.
    """

    @property
    def scopes(self):
        """
        :returns: an iterable of Scopes.
        :rtype: iterable of :class:`dockerauth.docker.Scope`
        """
        if hasattr(self,'_scopes'):
            return self._scopes
        _s = self.args.get("scope")
        if _s is None:
            return []
        if not isinstance(_s,list):
            _s = [ _s ]
        for i in range(0,len(_s)):
            if isinstance(_s[i],Scope):
                continue
            elif type(_s[i]) == list:
                _s[i] = Scope.fromlist(_s)
            else:
                _s[i] = Scope.fromstring(str(_s[i]))
        setattr(self,'_scopes',_s)
        return _s
    
    @property
    def account(self):
        return self.args.get("account")
        
    @property
    def client_id(self):
        return self.args.get("client_id")
        
    @property
    def service(self):
        return self.args.get("service")

class BasicAuthRequestMixin(object):
    """
    A request mixin that extracts HTTP Basic Auth username and password
    and presents them as properties.  py:type:`NoneType` usernames and
    passwords are allowed!
    """
    
    @property
    def authorization(self):
        if hasattr(self,'_authorization'):
            return self._authorization
        authstring = self.headers.get("Authorization")
        if not authstring:
            return None
        setattr(self,'_authorization',authstring)
        return authstring

    def __extract_basic_auth(self):
        authstring = self.authorization
        if not authstring:
            raise InvalidRequest("no Authorization header")
        [auth_type,auth_blob] = authstring.split(' ',1)
        if auth_type != "basic" and auth_type != "Basic":
            LOG.debug("%r: not using HTTP basic auth (%s)!"
                      % (repr(self),auth_type))
            raise InvalidRequest("not using HTTP Basic auth")
        bd = base64.b64decode(auth_blob)
        #
        # In python2, returns a binary str; in python3, returns a bytes.
        #
        if not isinstance(bd,str):
            bd = str(bd,'utf8')
        [username,password] = bd.split(":",1)
        return (username,password)
    
    @property
    def username(self):
        if hasattr(self,'_username'):
            return self._username
        try:
            (username,password) = self.__extract_basic_auth()
        except:
            return None
        setattr(self,'_username',username)
        setattr(self,'_password',password)
        return username
        
    @property
    def password(self):
        if hasattr(self,'_password'):
            return self._password
        try:
            (username,password) = self.__extract_basic_auth()
        except:
            return None
        setattr(self,'_username',username)
        setattr(self,'_password',password)
        return password

class CertRequestMixin(object):
    """
    A request mixin that extracts client x.509 certificates from
    commonly-used webserver and WSGI environments, and presents them as
    properties.
    """
    
    @property
    def client_cert(self):
        if hasattr(self,'_client_cert'):
            return self._client_cert
        raw = self.environ.get("SSL_CLIENT_CERT")
        if raw is None or raw == "":
            return None
        cc = load_pem_x509_certificate(str2bytes(raw),default_backend())
        setattr(self,'_client_cert',cc)
        return cc
    
    @property
    def client_cert_verified(self):
        if hasattr(self,'_client_cert_verified'):
            return self._client_cert_verified
        v = self.environ.get("SSL_CLIENT_VERIFY")
        if v is None or v == "":
            return False
        if v == "SUCCESS":
            v = True
        else:
            v = False
        setattr(self,'_client_cert_verified',v)
        return v

class DockerRegistryRequest(BaseRequest,DockerRequestMixin,
                            BasicAuthRequestMixin,CertRequestMixin):
    """
    A Docker registry request class that extends the
    :class:`BaseRequest` with several mixins.
    """

    def __repr__(self):
        return "DockerRegistryRequest(%s,%s,%s %s %s,%s,%s,%s)" \
          % (self.method,self.client_ip,self.uri,
             self.username,
             str(self.scopes),self.account,self.client_id,self.service)

class Response(object):

    def __init__(self,code,content="",headers=[]):
        self.code = code
        self.content = content
        self.headers = headers

    def __repr__(self):
        return "Response(%d,len(content)=%d,len(headers)=%d)" \
            % (self.code,len(self.content),len(list(self.headers.keys())))
