
import sys
import os
import logging
import importlib
from dockerauth.log import LOG
from dockerauth.config import (
    global_config_section, get_known_authn_sections, get_known_authz_sections,
    ConfigSection, ConfigError )
from dockerauth.util import isiterable
import hashlib
import base64
from cryptography.x509 import load_pem_x509_certificate
from cryptography.hazmat.primitives.serialization \
    import load_pem_public_key,load_pem_private_key
from cryptography.hazmat.primitives.serialization \
    import Encoding,PublicFormat
from cryptography.hazmat.backends import default_backend

@global_config_section
class ServerConfig(ConfigSection):
    name = "server"
    reconfig = False

    def __init__(self,configdir,jsonblob):
        super(ServerConfig,self).__init__(configdir,jsonblob)
        if "reconfigure" in jsonblob: self.reconfigure = jsonblob["reconfigure"]
        else:                         self.reconfigure = True
        if "threaded" in jsonblob:    self.threaded = jsonblob["threaded"]
        else:                         self.threaded = True
        if "host" in jsonblob:        self.host = jsonblob["host"]
        else:                         self.host = ""
        if "port" in jsonblob:        self.port = jsonblob["port"]
        else:                         self.port = 5001
        if "user" in jsonblob and jsonblob["user"]:
            try:
                i = int(jsonblob["user"])
                pwent = pwd.getpwuid(i)
                self.uid = pwent.pw_uid
                self.user = jsonblob["user"]
            except:
                try:
                    pwent = pwd.getpwnam(jsonblob["user"])
                    self.uid = pwent.pw_uid
                    self.user = jsonblob["user"]
                except:
                    raise ConfigError("could not find user '%s'" % (jsonblob["user"]))
        else:
            self.uid = None
            self.user = None
        if "group" in jsonblob and jsonblob["group"]:
            try:
                i = int(jsonblob["group"])
                grent = grp.getgrgid(i)
                self.gid = grent.gr_gid
                self.group = jsonblob["group"]
            except:
                try:
                    grent = grp.getgrnam(jsonblob["group"])
                    self.gid = grent.gr_gid
                    self.group = jsonblob["group"]
                except:
                    raise ConfigError("could not find group '%s'" % (jsonblob["group"]))
        else:
            self.gid = None
            self.group = None
        if "endpoint_url" in jsonblob:
            self.endpoint_url = jsonblob["endpoint_url"]
        else:
            self.endpoint_url = "/service/docker/token/v2"
        if "skip_builtins" in jsonblob and jsonblob["skip_builtins"] == True:
            self.skip_builtins = True
        else:
            self.skip_builtins = False
            import dockerauth.authn.builtins
            import dockerauth.authz.builtins
        self.extra_modules = []
        if "extra_modules" in jsonblob:
            if not isiterable(jsonblob["extra_modules"]):
                raise ConfigError("extra_modules field must be a list")
            for mod in jsonblob["extra_modules"]:
                self.extra_modules.append(importlib.import_module(mod))
        if "registry_addr" in jsonblob:
            self.registry_addr = jsonblob["registry_addr"]
        else:
            self.registry_addr = ""
        pass
        
    pass

@global_config_section
class LogConfig(ConfigSection):
    name = "log"
    reconfig = True

    def __init__(self,configdir,jsonblob={}):
        super(LogConfig,self).__init__(configdir,jsonblob)
        if "stderr" in jsonblob:
            self.stderr = bool(jsonblob["stderr"])
        else:
            self.stderr = False
        if "syslog" in jsonblob:
            self.syslog = jsonblob["syslog"]
        else:
            self.syslog = "dockerauth"
        if "file" in jsonblob:
            self.file = jsonblob["file"]
            if not os.access(self.file,os.W_OK):
                raise ConfigError("cannot write to log file '%s'" % (self.file,))
        else:
            self.file = None
        if "level" in jsonblob:
            l = jsonblob["level"]
            if l == "error" or l == "ERROR":
                self.level = logging.ERROR
            elif l == "warning" or l == "WARNING":
                self.level = logging.WARNING
            elif l == "info" or l == "INFO":
                self.level = logging.INFO
            elif l == "debug" or l == "DEBUG":
                self.level = logging.DEBUG
            else:
                raise ConfigError("unknown log level '%s'; must be one of" \
                                  " error,warning,info,debug" % (self.level))
        else:
            self.level = logging.WARNING
        if "flask_level" in jsonblob:
            l = jsonblob["flask_level"]
            if l == "error" or l == "ERROR":
                self.flask_level = logging.ERROR
            elif l == "warning" or l == "WARNING":
                self.flask_level = logging.WARNING
            elif l == "info" or l == "INFO":
                self.flask_level = logging.INFO
            elif l == "debug" or l == "DEBUG":
                self.flask_level = logging.DEBUG
            else:
                raise ConfigError("unknown log flask_level '%s'; must be one of" \
                                  " error,warn,info,debug" % (self.flask_level))
        else:
            self.flask_level = logging.WARNING
        pass

    def __eq__(self,other):
        if type(self) == type(other) \
            and self.syslog == other.syslog \
            and self.file == other.file \
            and self.level == other.level \
            and self.flask_level == other.flask_level:
            return True
        return False

    def apply(self,server,oldsection=None,oldobjects=None):
        flask_modules = ['flask','flask.Flask','werkzeug','werkzeug.serving']
        procname = os.path.basename(sys.argv[0])
        if oldobjects is not None:
            state = oldobjects
        else:
            state = {}
        if (oldsection and self.syslog != oldsection.syslog) or self.syslog:
            nh = None
            if self.syslog:
                if type(self.syslog) == bool:
                    nh = logging.handlers.SysLogHandler(address='/dev/log')
                else:
                    nh = logging.handlers.SysLogHandler(
                        address='/dev/log',facility=self.syslog)
                fm = logging.Formatter(
                    procname+"[%(process)d] %(name)s %(levelname)s %(message)s")
                nh.setFormatter(fm)
                LOG.addHandler(nh)
                for mod in flask_modules:
                    logging.getLogger(mod).addHandler(nh)
            if oldsection:
                LOG.removeHandler(state["syslog_h"])
                for mod in flask_modules:
                    logging.getLogger(mod).removeHandler(state["syslog_h"])
                del state["syslog_h"]
            if nh:
                state["syslog_h"] = nh
        if (oldsection and self.file != oldsection.file) or self.file:
            nh = None
            if self.file:
                nh = logging.FileHandler(self.file,mode='a')
                fm = logging.Formatter(
                    "%(asctime)s [%(process)d] %(levelname)-8s %(pathname)s"
                    ":%(lineno)d %(message)s")
                nh.setFormatter(fm)
                LOG.addHandler(nh)
                for mod in flask_modules:
                    logging.getLogger(mod).addHandler(nh)
            if oldsection:
                LOG.removeHandler(state["file_h"])
                for mod in flask_modules:
                    logging.getLogger(mod).removeHandler(state["file_h"])
                del state["file_h"]
            if nh:
                state["file_h"] = nh
        if server.standalone and (self.stderr or not self.file):
            # Once we open a console logger, it stays forever.
            if not "console_h" in state:
                nh = logging.StreamHandler()
                fm = logging.Formatter(
                    "%(asctime)s [%(process)d] %(levelname)-8s %(pathname)s"
                    ":%(lineno)d %(message)s")
                nh.setFormatter(fm)
                LOG.addHandler(nh)
                for mod in flask_modules:
                    logging.getLogger(mod).addHandler(nh)
                state["console_h"] = nh
        LOG.setLevel(self.level)
        for mod in flask_modules:
            logging.getLogger(mod).setLevel(self.flask_level)

        # Return the (possibly modified) state.
        return state

    pass

@global_config_section
class TlsConfig(ConfigSection):
    name = "tls"

    def __init__(self,configdir,jsonblob):
        super(TlsConfig,self).__init__(configdir,jsonblob)
        if "server_privkey_passphrase" in jsonblob:
            self.server_privkey_passphrase = jsonblob["server_privkey_passphrase"]
        else:
            self.signer_privkey_passphrase = None
        if "server_privkey" in jsonblob:
            try:
                filename = jsonblob["server_privkey"]
                if filename[0] != '/':
                    filename = self.configdir + "/" + filename
                fd = open(filename,'rb')
                contents = fd.read()
                fd.close()
                self.server_privkey = load_pem_private_key(
                    contents,self.server_privkey_passphrase,default_backend())
                self.server_privkey_file = filename
            except Exception as err:
                raise ConfigError("could not read 'server_privkey' (%s): %s"
                                  % (filename,str(err)))
        else:
            self.server_privkey = None
            self.server_privkey_file = None
        if "server_cert" in jsonblob:
            try:
                filename = jsonblob["server_cert"]
                if filename[0] != '/':
                    filename = self.configdir + "/" + filename
                fd = open(filename,'rb')
                contents = fd.read()
                fd.close()
                self.server_cert = load_pem_x509_certificate(contents,default_backend())
                self.server_cert_file = filename
            except Exception as err:
                raise ConfigError("could not read 'server_cert' (%s): %s"
                                % (filename,str(err)))
        else:
            self.server_cert = None
            self.server_cert_file = None

        if "signer_privkey_passphrase" in jsonblob:
            self.signer_privkey_passphrase = jsonblob["signer_privkey_passphrase"]
        else:
            self.signer_privkey_passphrase = None

        #
        # Use the server priv/pub keys if they didn't specify a separate
        # signing key.
        #
        if "signer_privkey" not in jsonblob and "signer_pubkey" not in jsonblob:
            LOG.info("using server privkey/pubkey to sign, since"
                     " signer_privkey and signer_pubkey are not specified")
            jsonblob["signer_privkey"] = jsonblob["server_privkey"]
            jsonblob["signer_pubkey"] = jsonblob["server_cert"]
            if "server_privkey_passphrase" in jsonblob:
                jsonblob["signer_privkey_passphrase"] = \
                    jsonblob["server_privkey_passphrase"]
            pass
        if "signer_privkey" not in jsonblob:
            raise ConfigError("no 'signer_privkey' file specified")
        try:
            filename = jsonblob["signer_privkey"]
            if filename[0] != '/':
                filename = self.configdir + "/" + filename
            fd = open(filename,'rb')
            contents = fd.read()
            fd.close()
            self.signer_privkey = load_pem_private_key(
                contents,self.signer_privkey_passphrase,default_backend())
            self.signer_privkey_file = filename
        except Exception as err:
            raise ConfigError("could not read 'signer_privkey' (%s): %s"
                              % (filename,str(err)))
        # Try to load either an x509 cert and extract the pubkey; or load
        # a PEM public key.
        if "signer_pubkey" not in jsonblob:
            raise ConfigError("no 'signer_pubkey' file specified")
        try:
            filename = jsonblob["signer_pubkey"]
            if filename[0] != '/':
                filename = self.configdir + "/" + filename
            fd = open(filename,'rb')
            contents = fd.read()
            fd.close()
        except Exception as err:
            raise ConfigError("could not read 'signer_pubkey' (%s): %s"
                              % (filename,str(err)))
        try:
            self.signer_cert = load_pem_x509_certificate(
                contents,default_backend())
            self.signer_pubkey = self.signer_cert.public_key()
            self.signer_pubkey_file = filename
        except Exception as e:
            try:
                self.signer_pubkey = load_pem_public_key(
                    contents,default_backend())
                self.signer_pubkey_file = filename
            except Exception as e2:
                raise ConfigError("could not read 'signer_pubkey' (%s) either"
                                  " as x509 cert or PEM public key: %s, %s"
                                  % (filename,str(e),str(e2)))
            pass

        # We will need this later to create JWT headers; so just do it now.
        # From https://docs.docker.com/registry/spec/auth/jwt/#getting-a-bearer-token
        # 1. Take the DER encoded public key which the JWT token was
        # signed against.
        pubkey_der = self.signer_pubkey.public_bytes(
            Encoding.DER,PublicFormat.SubjectPublicKeyInfo)
        # 2. Create a SHA256 hash out of it and truncate to 240bits.
        pubkey_der_hash = hashlib.sha256(pubkey_der).digest()
        # 3. Split the result into 12 base32 encoded groups with : as delimiter.
        b32 = base64.b32encode(pubkey_der_hash[:(int(240/8))])
        tl = []
        for i in range(0,48,4):
            tl.append(b32[i:(i+4)].decode())
        self.jwt_header_kid = ":".join(tl)
        pass
    pass

@global_config_section
class TokenConfig(ConfigSection):
    name = "token"
    reconfig = True

    def __init__(self,configdir,jsonblob):
        super(TokenConfig,self).__init__(configdir,jsonblob)
        if "issuer" in jsonblob:
            self.issuer = jsonblob["issuer"]
        else:
            raise ConfigError("must specify token:issuer same as for registry")
        if "service" in jsonblob:
            self.service = jsonblob["service"]
        else:
            raise ConfigError("must specify token:service same as for registry")
        if "exp" in jsonblob:
            self.exp = int(jsonblob["exp"])
        else:
            self.exp = 600
        if "slop" in jsonblob:
            self.slop = int(jsonblob["slop"])
        else:
            self.slop = 0
        if "utc" in jsonblob:
            if isinstance(jsonblob["utc"],bool):
                self.utc = jsonblob["utc"]
            elif isinstance(jsonblob["utc"],str):
                self.utc = bool(dockerreg.util.parse_bool_or_int(
                    jsonblob["utc"]))
            else:
                self.utc = bool(jsonblob["utc"])
        else:
            self.utc = False
        if "oauth_compat" in jsonblob:
            if isinstance(jsonblob["oauth_compat"],bool):
                self.oauth_compat = jsonblob["oauth_compat"]
            elif isinstance(jsonblob["oauth_compat"],str):
                self.oauth_compat = bool(dockerreg.util.parse_bool_or_int(
                    jsonblob["oauth_compat"]))
            else:
                self.oauth_compat = bool(jsonblob["oauth_compat"])
        else:
            self.oauth_compat = False
        pass
    pass

@global_config_section
class AuthnOrderConfig(ConfigSection):
    name = "authn_order"
    reconfig = True

    def __init__(self,configdir,jsonblob):
        super(AuthnOrderConfig,self).__init__(configdir,jsonblob)
        if type(jsonblob) != list or len(jsonblob) < 1:
            raise ConfigError("authn_order must be a non-empty list of section"
                              " names corresponding Authn backends")
        for method in jsonblob:
            if method not in get_known_authn_sections():
                raise ConfigError("bad authn_order method '%s'"
                                  % (str(method),))
        self.order = jsonblob
        return
    pass

@global_config_section
class AuthzOrderConfig(ConfigSection):
    name = "authz_order"
    reconfig = True

    def __init__(self,configdir,jsonblob):
        super(AuthzOrderConfig,self).__init__(configdir,jsonblob)
        if type(jsonblob) != list or len(jsonblob) < 1:
            raise ConfigError("authz_order must be a non-empty list of section"
                              " names corresponding Authz backends")
        for method in jsonblob:
            if method not in get_known_authz_sections():
                raise ConfigError("bad authz_order method '%s'"
                                  % (str(method),))
        self.order = jsonblob
        return
    pass
