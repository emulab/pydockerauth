import os
import sys
import time
import datetime
import dateutil.tz
import threading
import base64
import json
import jwt
from dockerauth.config import Config
import dockerauth.server.config
import dockerauth.util
from dockerauth.util.lock import RWLock
from dockerauth.log import LOG
from dockerauth.util import isiterable,utcoffset
from dockerauth.authn import ( Authn, AuthnError )
from dockerauth.authz import ( Authz, AuthzError )
from dockerauth.docker.scope import ( Scope, AuthScope )
from dockerauth.server.http import ( BaseRequest, Response )

class AuthServer(object):
    """
    An AuthServer authenticates users and serves them JWT tokens that
    authorize them to operate on a Docker registry, according to a
    `Config` object loaded from a config file.
    """
    def __init__(self,configfile,standalone=False):
        self.configfile = configfile
        self.standalone = standalone
        self.authns = {}
        self.authzs = {}
        self.reconfigure_thread = None
        self.check_and_reconfigure = False
        self.config_lock = RWLock()
        self.section_objects = {}

        self.config_lock.acquire_write()
        self.config = Config(self.configfile)
        self.config.apply(self)
        LOG.debug("authserver initial config:\n" + self.config.dump())
        self.config_lock.release()
        pass

    def check_and_reconfigure_once(self):
        st = self.config.is_stale()
        if st == False:
            return
        self.config_lock.acquire_write()
        st = self.config.is_stale()
        if st == False:
            self.config_lock.release()
            return
        LOG.debug("checking config at %f" % (time.time()))
        try:
            newconfig = self.config.reload()
            # We don't want to retry this config file
            self.config.timestamp = newconfig.timestamp
            LOG.debug("reloaded config:\n" + newconfig.dump())
        except Exception as e:
            # We don't want to retry this config file until it changes.
            self.config.timestamp = st
            LOG.error("failed to reload modified config file: %s" % (str(e),))
            LOG.exception(e)
        else:
            try:
                self.config.apply(self,newconfig=newconfig)
            except Exception as e:
                LOG.error("failed to load new, valid config (exiting!): %s"
                          % (str(e)))
                LOG.exception(e)
                sys.exit(1)
        self.config_lock.release()
        return

    def check_and_reconfigure_forever(self):
        while self.check_and_reconfigure:
            self.check_and_reconfigure_once()
            time.sleep(1)
            pass

    def spawn_reconfigure_thread(self):
        if self.reconfigure_thread is not None:
            LOG.error("already running config checker thread, not respawning!")
            return
        if not self.config.server.reconfigure == False:
            self.check_and_reconfigure = True
            self.reconfigure_thread = \
                threading.Thread(target=self.check_and_reconfigure_forever)
            self.reconfigure_thread.start()
            LOG.debug("spawned reconfigure thread")

    def interrupt_reconfigure_thread(self):
        if not self.reconfigure_thread:
            return
        self.check_and_reconfigure = False
        self.reconfigure_thread.join()
        self.reconfigure_thread = None

    def drop_privileges(self):
        """
        Drop privileges of this process according to the config.
        """
        uid = self.config.server.uid
        gid = self.config.server.gid
        if gid is not None and gid != 0:
            cgid = os.getegid()
            if cgid != gid:
                os.setgroups([])
                os.setgid(gid)
                LOG.info("dropped gid privilege to %d" % (os.getegid(),))
        if uid is not None and uid != 0:
            cuid = os.geteuid()
            if cuid != uid:
                os.setuid(uid)
                LOG.info("dropped uid privilege to %d" % (os.geteuid(),))

    def process_section_objects(self,section,objects):
        # Remove old objects from this section, including Authn/Authz modules.
        if section in self.section_objects:
            osol = self.section_objects[section]
            if osol is not None:
                if not isiterable(oso):
                    osol = [ osol ]
                for oso in osol:
                    if isinstance(oso,Authn):
                        LOG.info("removing old section '%s' Authn" % (section))
                        del self.authns[section]
                    if isinstance(oso,Authz):
                        LOG.info("removing old section '%s' Authz" % (section))
                        del self.authzs[section]
        # Add new objects:
        if objects is not None:
            if not isiterable(objects):
                osol = [ objects ]
            else:
                osol = objects
            for oso in osol:
                if isinstance(oso,Authn) and isinstance(oso,Authz):
                    LOG.info("inserting Authn/Authz '%s' for section '%s'"
                             % (str(oso),section))
                    self.authns[section] = oso
                else:
                    if isinstance(oso,Authn):
                        LOG.info("inserting Authn '%s' for section '%s'"
                                 % (str(oso),section))
                        self.authns[section] = oso
                    if isinstance(oso,Authz):
                        LOG.info("inserting Authz '%s' for section '%s'"
                                 % (str(oso),section))
                        self.authzs[section] = oso

    def get_section_objects(self,section):
        if section in self.section_objects:
            return self.section_objects[section]
        return None

    ## def set_auth_method(self,auth_obj):
    ##     if auth_obj.name in self.auth_methods:
    ##         LOG.debug("replacing auth backend object '%s'"
    ##                   % (str(self.auth_methods[auth_obj.name]),))
    ##     self.auth_method_order.append(auth_obj.name)
    ##     self.auth_methods[auth_obj.name] = auth_obj
    ##     LOG.info("installed auth backend '%s' (%s)"
    ##              % (str(auth_obj.name),str(auth_obj)))

    def authenticate(self,request,authns=[],**kwargs):
        """
        Authenticate using all the Authns we've been configured to use.
        This function does *not* catch exceptions from backends, so if a
        higher-priority backend raises an exception, the end result to
        the user will be HTTP 500.  It would be less secure to allow
        lower-prio backends to answer if a user should have been handled
        by a higher-prio backend.)  (A backend *can* throw an
        AuthnError, and the .message field of that exception will
        be sent back to the client; carefully consider the contents of
        that field!)

        :param request: an instance of :class:`dockerauth.server.http.BaseRequest` or a subclass
        :ptype request: :class:`dockerauth.server.http.BaseRequest`
        :param kwargs: any extra authentication parameters provided by the request handler
        :ptype kwargs: py:type:`dict`
        :return: throws an exception if an error occurs; returns False if none of the backends authenticated the user; returns True if a backend authenticated the user.
        :rtype: a tuple whose first member is a py:type:`bool`, :class:`dockerauth.authn.AuthnState`, or None; and whose second member is the :class:`dockerauth.authn.Authn` instance who authenticated or denied the user, or None.
        """
        for am in authns:
            LOG.debug("trying to authn user '%s' via %s (kwargs %s)"
                      % (request.username,str(am),str(kwargs)))
            ret = am.authenticate(request,**kwargs)
            if ret is None:
                continue
            elif ret is True or isinstance(ret,dockerauth.authn.AuthnState):
                LOG.info("auth backend '%s' authenticated user '%s' (%r)"
                         % (am.name,request.username,repr(ret)))
                return (ret,am)
            else:
                LOG.debug("auth backend '%s' denied user '%s'"
                          % (am.name,request.username))
                return (False,am)
        return (None,None)

    def authorize(self,request,authn=None,authn_state=None,authzs=[],**kwargs):
        """
        Authorizes an authenticated user to a list of AuthScopes.
        
        :param request: an instance of :class:`dockerauth.server.http.BaseRequest` or a subclass
        :ptype request: :class:`dockerauth.server.http.BaseRequest`
        :param authn: the :class:`dockerauth.authn.Authn` that authenticated the user.
        :ptype authn: :class:`dockerauth.authn.Authn`
        :param authn_state: the state (:class:`dockerauth.authn.AuthnState`) left by the authn, or None
        :ptype authn: :class:`dockerauth.authn.AuthnState`
        :param authzs: an ordered list of authz backends that may be used to authorize the user.
        :returns: None if no backend authorized the user, or AuthScope if one of the backends authenticates the user (AuthScope contains a token expiration, whether the token should be multi-use or one-time, and a list of Scopes that is the intersection of the client request and the scopes allowed to this account and context).
        """
        if authn is None:
            raise RuntimeError("authn must be specified!")
        for authz in authzs:
            ret = authz.authorize(request,authn=authn,authn_state=authn_state,**kwargs)
            if ret is None:
                continue
            elif isinstance(ret,AuthScope):
                return (ret,authz)
            else:
                raise RuntimeError("Authz backend '%s' returned an invalid value"
                                   % (az_name))
        LOG.info("no Authz backends matched user '%s'" % (request.username))
        return (None,None)

    def tokenize(self,request,ascope):
        """
        Find the intersection of the requested scopes, and the scopes in
        ascope.  After that, create and sign the token, and return it.
        We return the output of jwt.encode, which is a str in Python 2,
        and a bytes in Python 3.
        """
        intscopes = []
        for r in request.scopes:
            for a in ascope.scopes:
                #LOG.debug("%s %s" % (ascope,a))
                ret = r.intersect(a)
                if ret is not None and not ret in intscopes:
                    intscopes.append(ret)
            pass
        intascope = AuthScope(intscopes,exp=ascope.exp,
                              multiuse=ascope.multiuse)
        LOG.info("intersection of %s and %s -> %s"
                 % (str(request.scopes),str(ascope.scopes),str(intascope)))
        # Set an expiration if one isn't already set.
        if not intascope.exp:
            intascope.exp = self.config.token.exp
            pass
        # Construct the claim set.
        #
        # NB: the reference Docker registry is not compat with the JWT
        # spec, in its requirement for the 'exp' field.  JWT spec says
        # that is a UTC UNIX timestamp; however, the registry
        # implementation treats it as a local UNIX timestamp.  So, make
        # it configurable for future, but default (in Config) to
        # non-UTC.
        #
        if self.config.token.utc:
            uoffset = utcoffset()
        else:
            uoffset = 0
        now = int(time.time())
        claim = { 'iss': self.config.token.issuer,
                  'aud': self.config.token.service,
                  'sub': request.username,
                  'exp': now + intascope.exp + uoffset,
                  'nbf': now + uoffset - self.config.token.slop,
                  'iat': now + uoffset - self.config.token.slop,
                  'access': intascope.get_access_list() }
        if not ascope.multiuse:
            claim['jti'] = base64.b64encode(os.urandom(1024)).decode()
            pass
        headers = { 'kid':self.config.tls.jwt_header_kid }
        LOG.debug("claim = %s, headers = %s" % (claim,headers))
        token = jwt.encode(claim,self.config.tls.signer_privkey,headers=headers,
                           algorithm='RS256')
        expires_in = intascope.exp
        iat = datetime.datetime.fromtimestamp(now - self.config.token.slop)
        iat = iat.replace(tzinfo=dateutil.tz.tzlocal())
        iat = iat.astimezone(dateutil.tz.tzutc())
        issued_at = iat.isoformat()
        return (token,expires_in,issued_at)

    def _get_auth_backend_info(self):
        """
        A thread-safe approach to interacting with the current config.
        We want to handle a single request from the same Config object
        so that authn/authz processing is consistent.  This function
        returns a tuple of
        (authn_section_order_list,authn_section_backend_dict,authz_section_order_list,authz_section_backend_dict)
        constructed with the config lock held.
        """
        self.config_lock.acquire()
        (ordered_authns,ordered_authzs) = ([],[])
        for name in self.config.authn_order.order:
            if not name in self.authns:
                LOG.warn("Authn backend '%s' does not exist; skipping"
                         % (name))
                continue
            ordered_authns.append(self.authns[name])
        for name in self.config.authz_order.order:
            if not name in self.authzs:
                LOG.warn("Authz backend '%s' does not exist; skipping"
                         % (name))
                continue
            ordered_authzs.append(self.authzs[name])
        retval = ( ordered_authns,self.authns,ordered_authzs,self.authzs )
        self.config_lock.release()
        return retval
    
    def validate_request(self,request):
        if not request.authorization:
            LOG.warn("request %r has no Authorization header; refusing"
                     % (repr(request)))
            return Response(401,"requires Authorization header")

        if request.client_cert and not request.client_cert_verified:
            LOG.warn("request %r client cert not validated!"% (repr(request)))
            return Response(401,"invalid certificate")

    def handle_request(self,request,**kwargs):
        if not isinstance(request,BaseRequest):
            LOG.error("bug -- request (%r) must be subclass of dockerauth.server.http.BaseRequest" % (repr(request),))
            return Response(500,"internal server error")
        
        (ordered_authns,authns,ordered_authzs,authzs) = \
          self._get_auth_backend_info()
        resp = None

        LOG.debug("request %r" % (repr(request)))
        
        #
        # First, validate the request.
        #
        try:
            self.validate_request(request)
        except:
            LOG.error("failed to validate request '%r'" % (repr(request),))
            LOG.exception(sys.exc_info()[1])
            return Response(401,"malformed request")

        #
        # Second, try to authenticate.
        #
        authn_state = None
        try:
            username = request.username
            (ret,authn) = self.authenticate(
                request,authns=ordered_authns,**kwargs)
            if ret is None:
                LOG.info("no backends authenticated user '%s'" % (username))
                return Response(401,"unauthorized")
            elif type(ret) == bool and ret == False:
                LOG.info("backend %s denied user '%s'" % (authn,username))
                return Response(401,"unauthorized")
            elif type(ret) == bool and ret == True \
              or isinstance(ret,dockerauth.authn.AuthnState):
                LOG.info("backend %s authenticated user '%s' (%r)"
                         % (authn,username,repr(ret)))
                if isinstance(ret,dockerauth.authn.AuthnState):
                    authn_state = ret
            else:
                LOG.error("backend %s bug: %r not bool nor AuthnState!"
                          % (authn,repr(ret)))
                return Response(401,"unauthorized")
        except AuthnError as ame:
            LOG.error("error in authentication (message: %s) of user '%s'"
                      % (ame.message,username))
            LOG.exception(ame)
            return Response(500,"internal server error: " + ame.message)
        except:
            LOG.error("error in authentication")
            LOG.exception(sys.exc_info()[1])
            return Response(500,"internal server error")

        #
        # Third, try to authorize.
        #
        try:
            # Sometimes the authn that authenticated the user will place
            # conditions on which Authzs can be used to authorize (i.e.,
            # a combined Authn/Authz class might insist on handling that
            # user itself; other setups might allow a combination of
            # Authn and one or more Authzs; or they might not specify
            # any particular Authz, in which case we traverse the entire
            # set).
            rauthzs = ordered_authzs
            if authn.allowed_authzs == [ authn ] \
              or authn.allowed_authzs == authn:
#               \
#              or authn.allowed_authzs == [ None ] \
#              or authn.allowed_authzs == None:
                LOG.info("limiting authorization for user '%s' to authn %r"
                         % (username,repr(authn)))
                rauthzs = [ authn ]
            elif isiterable(authn.allowed_authzs):
                rauthzs = []
                for name in authn.allowed_authz:
                    if not name in authzs:
                        LOG.warn("Authz backend '%s' does not exist; skipping"
                                 % (name))
                        continue
                    rauthzs.append(authzs[name])
            elif authn.allowed_authzs != None:
                LOG.warn("bad configuration for allowed_authzs '%s' in Authn %s;"
                         " ignoring!" % (str(authn.allowed_authzs),authn.name))
            (ascope,authz) = self.authorize(
                request,authn=authn,authn_state=authn_state,authzs=rauthzs,
                **kwargs)
            if ascope is None:
                LOG.info("no backends authorized user '%s'" % (username))
                return Response(401,"unauthorized")
            elif not isinstance(ascope,AuthScope):
                LOG.error("backend %s bug: %s not AuthScope!" % (authz,ascope))
                return Response(401,"unauthorized")
            else:
                LOG.info("backend %s authorized user '%s' to %s"
                         % (authz,username,ascope))
        except AuthzError as ame:
            LOG.error("error in authorization (message: %s) of user '%s'"
                      % (ame.message,username))
            LOG.exception(ame)
            return Response(500,"internal server error: " + ame.message)
        except:
            LOG.error("error in authorization")
            LOG.exception(sys.exc_info()[1])
            return Response(500,"internal server error")

        #
        # Finally, tokenize the authorized scopes, and return the token.
        #
        try:
            (token,expires_in,issued_at) = self.tokenize(request,ascope)
            token = token.decode()
            if self.config.token.oauth_compat:
                token_blob = dict(access_token=token)
            else:
                token_blob = dict(token=token)
            if expires_in is not None:
                token_blob["expires_in"] = expires_in
            if issued_at is not None:
                token_blob["issued_at"] = issued_at
            jsonblob = json.dumps(token_blob,indent=None,separators=(',',':'))
            LOG.debug("token blob: %s" % (jsonblob))
            resp = Response(200,jsonblob,{'Content-Type':'application/json'})
            LOG.info("authenticated response: %r" % (repr(resp),))
            return resp
        except:
            LOG.error("error while tokenizing")
            LOG.exception(sys.exc_info()[1])
            return Response(500,"internal server error")

        # That's it; we handled the return in the final try: block.
        pass
    
    pass
