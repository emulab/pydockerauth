import six
import os
from dockerauth.config import ConfigError

class InvalidScope(ConfigError):
    pass

class Scope(object):
    """
    A wrapper for a Docker scope authorization.  Scopes are quite
    limited in Docker registry v2.  A scope is a three-tuple, where the
    first item is the type of registry object being authorized; the
    third is the name of the scope (or the wildcard "*"); the third is
    the set of actions that the recipient of the scope may perform.
    """
    _valid_map = { 'registry':['*'],
                   'repository':['push','pull','delete','*'] }

    def __init__(self,type,name,actions):
        self.lst = [type,name,actions]
        (self.type,self.name,self.actions) = Scope._validate(self.lst)
        pass
    
    @staticmethod
    def fromstring(x):
        lidx = x.find(":")
        if lidx < 0:
            raise InvalidScope(
                "invalid scope string: not colon-separated 3-tuple (%s)"
                % (str(x),))
        ridx = x.rfind(":")
        if ridx == lidx:
            raise InvalidScope(
                "invalid scope string: not colon-separated 3-tuple (%s)"
                % (str(x),))
        xl = [ x[0:lidx],x[lidx+1:ridx],x[ridx+1:].split(',') ]
        return Scope(*xl)

    @staticmethod
    def fromlist(lst):
        (type,name,actions) = Scope._validate(lst)
        return Scope(type,name,actions)

    def intersect(self,os):
        # We can only have one type per scope, so this is trivial.
        if self.type != os.type:
            return None
        # Ensure we only keep the common actions.
        if "*" in self.actions and "*" in os.actions:
            tmpactions = ["*"]
        elif "*" in self.actions:
            tmpactions = os.actions
        elif "*" in os.actions:
            tmpactions = self.actions
        else:
            tmpactions = []
            for x in self.actions:
                if x in os.actions:
                    tmpactions.append(x)
                pass
        if len(tmpactions) == 0:
            return None
        # Finally, the "name" field.  This is a bit trickier because
        # wildcards are allowed.  Of course, the spec doesn't say how
        # wildcards behave in names.  So we assume that paths may end in
        # wildcards at any time.  If self.name is *, and os.name is
        # a/b/c, we will return a/b/c as the intersection.  If self.name
        # is a/b/*, and os.name is a/b/c, we will return a/b/c.  If
        # self.name is a/b/c*, and os.name is a/b/c, we will return
        # a/b/c.  If self.name is a/b/c/* and os.name is a/b/c, we will
        # return None, because we will not strip the / from self.name
        # before search for that as a common prefix in os.name .
        tmpname = None
        if self.name == os.name:
            tmpname = self.name
        elif self.name == "*":
            tmpname = os.name
        elif os.name == "*":
            tmpname = self.name
        elif self.name[-1:] == "*" and os.name.startswith(self.name[:-1]):
            tmpname = os.name
        elif os.name[-1:] == "*" and self.name.startswith(os.name[:-1]):
            tmpname = self.name
        elif self.name[-2:] == "/*" and os.name.startswith(self.name[:-2]):
            tmpname = os.name
        elif os.name[-2:] == "/*" and self.name.startswith(os.name[:-2]):
            tmpname = self.name
            pass
        if tmpname is None:
            return None
        
        return Scope(self.type,tmpname,tmpactions)

    @staticmethod
    def _validate(lst):
        if type(lst) != list:
            raise InvalidScope("invalid scope: not list (%s)" % (str(lst),))
        if len(lst) != 3:
            raise InvalidScope("invalid scope: not 3 elements (%s)"
                               % (str(lst),))
        if lst[0] not in Scope._valid_map:
            raise InvalidScope("scope[0] invalid type %s (%s)"
                               % (lst[0],str(lst),))
        actarg = lst[2]
        if isinstance(actarg,six.string_types) and actarg.find(',') > -1:
            actarg = actarg.split(',')
        if type(actarg) == list:
            for x in actarg:
                if x not in Scope._valid_map[lst[0]]:
                    raise InvalidScope("scope[2] invalid action %s (%s)"
                                       % (x,str(lst),))
            actions = actarg
        else:
            if actarg not in Scope._valid_map[lst[0]]:
                raise InvalidScope("scope[2] invalid action %s (%s)"
                                   % (actarg,str(lst),))
            actions = [ actarg ]
        return (lst[0],lst[1],actions)

    def __eq__(self,os):
        if self.type != os.type:
            return False
        if self.name != os.name:
            return False
        if self.actions != os.actions:
            return False
        return True

    def __repr__(self):
        return "Scope(%s,%s,%s)" % (self.type,self.name,str(self.actions))

    pass

class AuthScope(object):
    """
    An AuthScope is one or more Scopes that a user is authorized to,
    coupled to an optional expiration time, and whether this scope
    should be issued as a multi-use token, or a single-use token.  These
    are the only bits of the token we allow a particular backend to
    control.
    """
    def __init__(self,scopes,exp=None,multiuse=False):
        self.scopes = scopes
        self.exp = exp
        self.multiuse = multiuse
        pass

    def get_access_list(self):
        return list([{ "type":x.type,"name":x.name,
                       "actions":x.actions } for x in self.scopes])

    @staticmethod
    def join(auth_scope_list):
        """
        Combine multiple AuthScopes into a single scope.  This means we
        take multiple AuthScopes in a list, each potentially with
        different expiration and multiuse fields, and combine all the
        scopes into one AuthScope object with the minimum of all
        expirations, and set multiuse only if all AuthScope objects in
        the list had multiuse set.  Thus, this is the least permission
        joining of all Scopes in the AuthScope list.
        """
        exp = None
        multiuse = None
        scopes = []
        for asi in auth_scope_list:
            if multiuse is None:
                multiuse = asi.multiuse
            else:
                multiuse = multiuse and asi.multiuse
            if asi.exp is not None:
                if exp == None:
                    exp = asi.exp
                elif asi.exp < exp:
                    exp = asi.exp
            scopes.extend(asi.scopes)
            pass
        return AuthScope(scopes,exp=exp,multiuse=multiuse)

    def __repr__(self):
        return "AuthScope(exp=%s,multiuse=%s,scopes=%s)" \
            % (str(self.exp),str(self.multiuse),str(self.scopes))
    pass
