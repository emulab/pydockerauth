import six
import sys
from dockerauth.log import LOG
from dockerauth.config import (
    global_config_section, ConfigSection, ConfigError )
from dockerauth.authn import (
    Authn, AuthnError, InternalAuthnError )

class User(object):
    def __init__(self,name,udict):
        self.name = name
        if "password" not in udict:
            raise ConfigError("password not specified for user '%s'" % (name,))
        self.password = udict["password"]
        pass

    def __repr__(self):
        return "User(%s)" % (self.name)

    pass

class BasicUserAuth(Authn):
    name = "BasicUserAuth"

    def __init__(self,section_config,server):
        Authn.__init__(self,section_config,server)
        if not isinstance(section_config,BasicUserConfig):
            raise ConfigError("expecting BasicUserConfig config section")
        self.users = section_config.users
        pass

    def authenticate(self,request,**kwargs):
        (username,password) = (request.username,request.password)
        if not request.username in self.users:
            return None
        try:
            # Our hash is from JSON so it will be a unicode in Python2,
            # and a str (which is unicode) in Python3.  Either way, we
            # need to encode it first.
            hsh = self.users[request.username].password
            #if not type(hsh) == str:
            #    bd = str(hsh,'utf8')
            return Authn.checkpass(password,hsh)
        except:
            LOG.exception("failed to check passwd hash for user '%s'; possible"
                          " unsupported/malformed hash")
            six.reraise(*sys.exc_info())

    def __repr__(self):
        return "StaticAuth(numusers=%d)" % (len(list(self.users.keys())))

    pass

@global_config_section
class BasicUserConfig(ConfigSection):
    name = "users"
    reconfig = True
    authnclass = BasicUserAuth

    def __init__(self,configdir,jsonblob):
        super(BasicUserConfig,self).__init__(configdir,jsonblob)
        self.users = {}
        if type(jsonblob) != dict:
            raise ConfigError("'users' config item must be a dict of username to dict")
        for (u,v) in six.iteritems(jsonblob):
            if u in self.users:
                raise ConfigError("duplicate user %r" % (u,))
            self.users[u] = User(u,v)
        pass

    def __repr__(self):
        return "BasicUserConfig(numusers=%s)" % (len(list(self.users.keys())))

    def dump(self,full=False,indent=0):
        def __newline():
            return "\n" + " " * (indent+2)
        retval = str(self) + ": users=["
        for v in list(self.users.values()):
            retval +=  __newline() + str(v)
        return retval

    pass
