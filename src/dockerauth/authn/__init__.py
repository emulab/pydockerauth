import six
import abc
import crypt
from dockerauth.config import ConfigError

class InternalAuthnError(Exception):
    """
    :class:`Authn` subclasses should throw errors of this type if they
    want them logged, but *not* returned to the user in the HTTP 500
    response.
    """
    pass

class AuthnError(Exception):
    """
    :class:`Authn` subclasses should throw errors of this type if they
    want them logged, *and* if they want the message field returned to
    the user in the HTTP 500 response.
    """
    pass

@six.add_metaclass(abc.ABCMeta)
class AuthnState(object):
    """
    A subclass instance of this class may be returned by :func:`dockerauth.authn.Authn.authenticate` instead of a boolean, so that state may be carried between :func:`Authn.authenticate` and :func:`dockerauth.authz.Authz.authorize`.
    """

@six.add_metaclass(abc.ABCMeta)
class Authn(object):
    name = "Authn"
    standalone = False
    allowed_authzs = None

    def __init__(self,section_config,server):
        """
        Instantiate a new :class:`Authn` object.

        :param section_config: A :class:`dockerauth.config.ConfigSection` object
        :param server: A reference to the :class:`dockerauth.server.auth.AuthServer` object creating this instance.
        """
        self.config = section_config
        self.server = server
        self.allowed_authzs = None
        if self.__class__.standalone:
            self.allowed_authzs = [ self ]
        elif hasattr(section_config,"allowed_authz"):
            aas = section_config.allowed_authzs
            if aas == None:
                self.allowed_authzs = self
            elif isiterable(aas):
                self.allowed_authzs = aas
            else:
                raise ConfigError("allowed_authzs may be None or a list of"
                                  " authz section names")
        pass

    @abc.abstractmethod
    def authenticate(self,request,**kwargs):
        """
        Authenticate a user.  Subclasses *can* throw an :class:`AuthnError`,
        and the ``message`` field of that exception will be sent back to the
        client; carefully consider the contents of that field!  If
        subclasses simply want the error to be logged, they should throw
        an :class:`InternalAuthnError` or some other exception; no
        detail will be sent to the user in that case.

        :param request: an instance of :class:`dockerauth.server.http.BaseRequest` or a subclass
        :ptype request: :class:`dockerauth.server.http.BaseRequest`
        :param kwargs: any extra authentication parameters provided by the request handler
        :type kwargs: py:type:`dict`
        :returns: ``False`` if the user cannot be authenticated; ``True`` if the user is authenticated; ``None`` if the user does not exist; throws an exception if an error occurs.
        :rtype: bool
        :rtype: None
        :rtype: :class:`dockerauth.authn.AuthnState`
        """

    @staticmethod
    def checkpass(plaintext,encoded):
        """
        Uses :func:`crypt.crypt` or :func:`bcrypt.hashpw` to encrypt the
        supplied plaintext password against the encoded, modular crypt'd
        password hash.  We don't require bcrypt, but if you supply a
        bcrypt'd hash, you'll get exceptions in your log until you've
        installed it.  Finally, for bcrypt, we transform unicode
        plaintext and encoded using :func:`six.b` to a binary
        non-unicodeobject, so that this will just work on Python2/3
        (under 3, plaintext and encoded will be str (unicode); under 2,
        they would be str (binary)).  ``crypt.crypt`` or
        ``bcrypt.hashpw`` may throw exceptions on malformed or
        unsupported hashes; we pass those through if they occur.

        :param plaintext: A plaintext password string to encrypt
        :type plaintext: str
        :param encoded: A password hash string that must match the encrypted :param plaintext:
        :type encoded: str
        :returns: True if plaintext matches encoded; False otherwise.
        :rtype: bool
        """
        sidx = encoded.rindex('$')
        if sidx < 2:
            raise ConfigError("invalid password hash!")
        saltsubstr = encoded[0:(sidx+1)]
        if saltsubstr[0:4] in ["$2a$","$2b$","$2y$"]:
            import bcrypt
            # Support both Python 2/3.  bcrypt.hashpw needs a binary
            # string, and this is a quick way to get it.  The whacko
            # thing is in Python2, six.b insists on taking a string
            # literal (i.e., not a unicode object), so we force that via
            # a simple str() conv.  I hope.
            (_p,_e) = (six.b(str(plaintext)),six.b(str(encoded)))
            ret = bcrypt.hashpw(_p,_e)
            if ret == _e:
                return True
        elif crypt.crypt(plaintext,saltsubstr) == encoded:
            return True
        return False

    pass
