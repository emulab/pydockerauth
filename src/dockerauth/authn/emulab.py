import six
import sys
import threading
from dockerauth.config import (
    global_config_section, ConfigSection, ConfigError )
from dockerauth.log import LOG
from dockerauth.util.lock import RWLock
from dockerauth.authn import Authn
from dockerauth.authn import (
    Authn, AuthnError, InternalAuthnError )
from dockerauth.authn import ( Authn, AuthnState )
from dockerauth.authz import (
    Authz, AuthzError, InternalAuthzError )
from dockerauth.util.ip import IPAddress
from dockerauth.docker.scope import ( Scope, AuthScope )
from dockerauth.util.mysql import ThreadedMysqlWrapper
from dockerauth.util.auth import Cert

class EmulabAuthnState(AuthnState):
    def __init__(self,realusername,username,scope_type,scope_value=None,
                 token_lifetime=None,token_onetime=False,system=0,isadmin=False):
        self.realusername = realusername
        self.username = username
        self.scope_type = scope_type
        self.scope_value = scope_value
        self.token_lifetime = token_lifetime
        self.token_onetime = token_onetime
        self.system = system
        self.isadmin = isadmin

    def __repr__(self):
        isastr = ""
        if self.isadmin:
            isastr = ",admin=True"
        return "<%s %s(%s),%s,%s,%s,%s,%s%s>" \
          % (self.__class__.__name__,self.username,self.realusername,
             str(self.scope_type),str(self.scope_value),str(self.token_lifetime),
             str(self.token_onetime),str(self.system),isastr)

class EmulabAuth(Authn,Authz):
    name = "EmulabAuth"
    standalone = True

    def __init__(self,section_config,server):
        Authn.__init__(self,section_config,server)
        Authz.__init__(self,section_config,server)
        if type(section_config) != EmulabConfig:
            raise ConfigError("expecting EmulabConfig")
        self.config = section_config
        # MySQLdb is fussy about None input args; pymysql is not.
        # So be careful.
        self.cargs = dict(db=self.config.db)
        if self.config.host is not None:
            self.cargs["host"] = self.config.host
        if self.config.port is not None:
            self.cargs["port"] = self.config.port
        if self.config.unix_socket is not None:
            self.cargs["unix_socket"] = self.config.unix_socket
        if self.config.user is not None and self.config.user != "":
            self.cargs["user"] = self.config.user
        if self.config.passwd is not None:
            self.cargs["passwd"] = self.config.passwd
        #LOG.debug("connection args: %s" % (str(cargs)))
        kwargs = {}
        if not "user" in self.cargs:
            kwargs["fakeuserbase"] = "dockerauth"
        self.db = ThreadedMysqlWrapper(self.cargs,**kwargs)

    def authenticate(self,request,**kwargs):
        (username,password) = (request.username,request.password)
        qres = []
        ret = None
        # First, check the users table.  Only if the user/pass match the
        # users table do we consider the admin bit!
        try:
            qres = self.db.query(
                "select usr_pswd,admin from users where uid=%s",(username,))
        except:
            (extype,ex,extb) = sys.exc_info()
            LOG.exception(ex)
            raise InternalAuthnError("mysql error: %s" % (str(ex)))
        if len(qres) > 0:
            (hsh,admin) = (qres[0][0],qres[0][1])
            # If there's a cert, and it matches a valid cert in the
            # user_sslcerts table, we accept that in lieu of a password.
            # If the cert isn't in our DB, though, we return an
            # immediate error.
            if request.client_cert and request.client_cert_verified:
                qres = []
                ccert = None
                try:
                    qres = self.db.query(
                        "select cert from user_sslcerts where"
                        "  uid=%s and revoked is NULL and NOW() < expires",
                        (username,))
                except:
                    (extype,ex,extb) = sys.exc_info()
                    LOG.exception(ex)
                    raise InternalAuthnError("mysql error: %s" % (str(ex)))
                if qres and len(qres) > 0:
                    try:
                        ccert = Cert(request.client_cert)
                        for [user_cert,] in qres:
                            ucert = Cert.fromstring(
                                "%s\n%s%s\n" % ("-----BEGIN CERTIFICATE-----",
                                                user_cert,
                                                "-----END CERTIFICATE-----"))
                            if ucert.fingerprint == ccert.fingerprint:
                                LOG.debug("authenticated user %s by cert %s"
                                          % (username,ucert.fingerprint))
                                return EmulabAuthnState(
                                    username,username,"user",isadmin=admin)
                    except:
                        (extype,ex,extb) = sys.exc_info()
                        LOG.exception(ex)
                        raise InternalAuthnError(
                            "certificate error: %s" % (str(ex)))
            # Ok, fall back to checking the password.
            try:
                ret = Authn.checkpass(password,hsh)
                if ret == True:
                    return EmulabAuthnState(
                        username,username,"user",isadmin=admin)
            except:
                (extype,ex,extb) = sys.exc_info()
                LOG.exception(ex)
                raise InternalAuthnError("checkpass error: %s" % (str(ex)))
        # If we didn't find a match in the users table, check the token
        # table:
        try:
            qres = self.db.query(
                "select uid,hash,scope_type,scope_value,"
                "  token_lifetime,token_onetime,system"
                " from user_token_passwords where username=%s"
                "  and subsystem='docker.registry'"
                "  and NOW() >= issued"
                "  and (expiration is NULL or NOW() < expiration)",
                (username,))
        except:
            (extype,ex,extb) = sys.exc_info()
            LOG.exception(ex)
            raise InternalAuthnError("mysql error: %s" % (str(ex)))
        for rres in qres:
            try:
                ret = Authn.checkpass(password,rres[1])
                if ret:
                    return EmulabAuthnState(
                        rres[0],username,rres[2],scope_value=rres[3],
                        token_lifetime=rres[4],token_onetime=rres[5],
                        system=rres[6],isadmin=False)
            except:
                (extype,ex,extb) = sys.exc_info()
                LOG.exception(ex)
                raise InternalAuthnError("checkpass error: %s" % (str(ex)))
        # See if this is an authentication from a node that hosts
        # VMs, one or more of which has reloads scheduled.
        try:
            regaddr = self.server.config.server.registry_addr
            if not regaddr:
                raise InternalAuthnError(
                    "no server.registry_addr configured; check config file!")
            if request.client_ip:
                if not isinstance(request.client_ip,IPAddress):
                    src = IPAddress.parse(request.client_ip)
                else:
                    src = request.client_ip
            else:
                raise InternalAuthnError("no client_ip in request!")
            qres = self.db.query(
                "select u.uid,n.node_id,i.node_id,iv.path,"
                "  iv.imagename,cr.image_id,cr.imageid_version"
                " from interfaces as i"
                "  left join nodes as n on i.node_id=n.phys_nodeid"
                "  left join current_reloads as cr on n.node_id=cr.node_id"
                "  left join image_versions as iv on"
                "   (cr.image_id=iv.imageid and cr.imageid_version=iv.version)"
                "  left join reserved as r on i.node_id=r.node_id"
                "  left join experiments as e on r.exptidx=e.idx"
                "  left join users as u on e.creator_idx=u.uid_idx"
                " where i.IP=%s and i.node_id=%s and cr.image_id is not NULL"
                "  and iv.format='docker' and e.eventkey=%s",
                (src.string,username,password))
            repos = []
            if qres and len(qres) > 0:
                for row in qres:
                    (creator,vnode_id,physnode_id,path,imagename,image_id,imageid_version) = row
                    if not path or not path.startswith(regaddr):
                        continue
                    ridx = path.find("/")
                    cidx = path.rfind(":")
                    if ridx < 0 or cidx < 0 or cidx <= ridx:
                        continue
                    repo = path[(ridx+1):cidx]
                    repos.append(repo)
                return EmulabAuthnState(creator,username,"node.pull",repos,
                                        token_lifetime=3600,system=1,isadmin=False)
        except:
            (extype,ex,extb) = sys.exc_info()
            LOG.exception(ex)
            raise InternalAuthnError("node-current_reloads error: %s" % (str(ex)))
        # Finally, see if this is an authentication from a node that hosts
        # VMs that does not have any reloads scheduled but has images loaded:
        try:
            regaddr = self.server.config.server.registry_addr
            if not regaddr:
                raise InternalAuthnError(
                    "no server.registry_addr configured; check config file!")
            if request.client_ip:
                if not isinstance(request.client_ip,IPAddress):
                    src = IPAddress.parse(request.client_ip)
                else:
                    src = request.client_ip
            else:
                raise InternalAuthnError("no client_ip in request!")
            qres = self.db.query(
                "select u.uid,n.node_id,i.node_id,iv.path,"
                "  iv.imagename,p.imageid,p.imageid_version"
                " from interfaces as i"
                "  left join nodes as n on i.node_id=n.phys_nodeid"
                "  left join partitions as p on n.node_id=p.node_id"
                "  left join image_versions as iv on"
                "   (p.imageid=iv.imageid and p.imageid_version=iv.version)"
                "  left join reserved as r on i.node_id=r.node_id"
                "  left join experiments as e on r.exptidx=e.idx"
                "  left join users as u on e.creator_idx=u.uid_idx"
                " where i.IP=%s and i.node_id=%s and p.imageid is not NULL"
                "  and iv.format='docker' and e.eventkey=%s",
                (src.string,username,password))
            repos = []
            if qres and len(qres) > 0:
                for row in qres:
                    (creator,vnode_id,physnode_id,path,imagename,image_id,imageid_version) = row
                    if not path or not path.startswith(regaddr):
                        continue
                    ridx = path.find("/")
                    cidx = path.rfind(":")
                    if ridx < 0 or cidx < 0 or cidx <= ridx:
                        continue
                    repo = path[(ridx+1):cidx]
                    repos.append(repo)
                return EmulabAuthnState(creator,username,"node.pull",repos,
                                        token_lifetime=3600,system=1,isadmin=False)
        except:
            (extype,ex,extb) = sys.exc_info()
            LOG.exception(ex)
            raise InternalAuthnError("node-partitions-loaded error: %s" % (str(ex)))
        # At this point, we are either returning None (no such user), or
        # False (bad password).
        return ret

    def authorize(self,request,authn_state=None,**kwargs):
        if authn_state is None or not isinstance(authn_state,EmulabAuthnState):
            raise InternalAuthnError(
                "bad type of authn_state (should be EmulabAuthnState);"
                " misconfiguration?")
        username = request.username
        if authn_state.scope_type == "user":
            try:
                qres = self.db.query(
                    "select gm.gid,gm.pid,gm.trust from users as u"
                    " left join group_membership as gm on u.uid=gm.uid"
                    " where u.uid=%s",
                    (username,))
            except:
                (extype,ex,extb) = sys.exc_info()
                LOG.exception(ex)
                raise InternalAuthzError("mysql error: %s" % (str(ex)))
            scopes = []
            for row in qres:
                (gid,pid,trust) = row
                #
                # Docker only allows lowercase repo names; munge ours
                # that allow uppercase.
                #
                (gid,pid) = (gid.lower(),pid.lower())
                if trust in ['local_root','group_root','proj_root']:
                    scopes.append(
                        Scope('repository','%s/%s/*' % (pid,gid),['*']))
                elif trust == 'user':
                    scopes.append(
                        Scope('repository','%s/%s/*' % (pid,gid),['pull']))
            if authn_state.isadmin:
                scopes.append(Scope('registry','catalog',['*']))
            if authn_state.isadmin and self.config.adminexp:
                exp = self.config.adminexp
            else:
                exp = self.config.exp
            return AuthScope(scopes,exp=exp,multiuse=False)
        elif authn_state.scope_type == "image.create":
            # In this case, scope_value must have a reponame (i.e.,
            # pid/eid/imagename) that we just stuff into the auth with
            # permission to push.
            if not authn_state.scope_value:
                raise InternalAuthnError(
                    "bad scope_value for scope_type image.create")
            mu = not authn_state.token_onetime
            scopes = [ Scope('repository',authn_state.scope_value,['push','pull']) ]
            return AuthScope(scopes,exp=authn_state.token_lifetime,multiuse=mu)
        elif authn_state.scope_type == "image.pull":
            # In this case, scope_value must have a reponame (i.e.,
            # pid/eid/imagename) that we just stuff into the auth with
            # permission to pull.
            if not authn_state.scope_value:
                raise InternalAuthnError(
                    "bad scope_value for scope_type image.pull")
            mu = not authn_state.token_onetime
            scopes = [ Scope('repository',authn_state.scope_value,['pull']) ]
            return AuthScope(scopes,exp=authn_state.token_lifetime,multiuse=mu)
        elif authn_state.scope_type == "image.rename" \
            or authn_state.scope_type == "image.delete":
            # In this case, scope_value must have a reponame (i.e.,
            # pid/eid/imagename) that we just stuff into the auth with
            # permission to push.
            if not authn_state.scope_value:
                raise InternalAuthnError(
                    "bad scope_value for scope_type image.rename/delete")
            mu = not authn_state.token_onetime
            scopes = [ Scope('repository',authn_state.scope_value,['push','pull','*']) ]
        elif authn_state.scope_type == "node.pull":
            # In this case, a local node is validly trying to pull an image.
            if not authn_state.scope_value:
                raise InternalAuthnError(
                    "bad scope_value %s for scope_type node.pull" % str(authn_state.scope_value))
            mu = not authn_state.token_onetime
            if type(authn_state.scope_value) == list:
                scopes = []
                for x in authn_state.scope_value:
                    scopes.append(Scope('repository',x,['pull']))
            else:
                scopes = [ Scope('repository',authn_state.scope_value,['pull']) ]
            return AuthScope(scopes,exp=authn_state.token_lifetime,multiuse=mu)
        else:
            raise InternalAuthnError(
                "unsupported scope_type %s" % (authn_state.scope_type))
    pass

@global_config_section
class EmulabConfig(ConfigSection):
    name = "emulab"
    reconfig = True
    authnclass = EmulabAuth
    authzclass = EmulabAuth

    def __init__(self,configdir,jsonblob):
        super(EmulabConfig,self).__init__(configdir,jsonblob)
        if not "db" in jsonblob:       self.db = "tbdb"
        else:                          self.db = jsonblob["db"]
        if not "host" in jsonblob:     self.host = None
        else:                          self.host = jsonblob["host"]
        if not "port" in jsonblob:     self.port = None
        else:                          self.port = jsonblob["port"]
        if not "unix_socket" in jsonblob: self.unix_socket = None
        else:                             self.unix_socket = jsonblob["unix_socket"]
        if not "user" in jsonblob:     self.user = None
        else:                          self.user = jsonblob["user"]
        if not "passwd" in jsonblob:   self.passwd = None
        else:                          self.passwd = jsonblob["passwd"]
        if not "exp" in jsonblob:      self.exp = None
        else:                          self.exp = jsonblob["exp"]
        if not "adminexp" in jsonblob: self.adminexp = None
        else:                          self.adminexp = jsonblob["adminexp"]
        pass

    def __repr__(self):
        return "EmulabConfig(%s@%s:%s)" % (self.user,self.host,self.db)
    pass
