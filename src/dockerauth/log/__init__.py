import logging

# Default to something sane for our package
LOG = logging.getLogger('dockerauth')

def setLogger(new_logger):
    global LOG
    LOG.debug("switching from current logger %r to new logger %r"
                 % (repr(LOG),repr(new_logger)))
    LOG = new_logger

def getLogger():
    return LOG
