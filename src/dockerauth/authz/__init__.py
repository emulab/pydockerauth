import six
import abc

class InternalAuthzError(Exception):
    """
    :class:`Authz` subclasses should throw errors of this type if they
    want them logged, but *not* returned to the user in the HTTP 500
    response.
    """
    pass

class AuthzError(Exception):
    """
    :class:`Authz` subclasses should throw errors of this type if they
    want them logged, *and* if they want the message field returned to
    the user in the HTTP 500 response.
    """
    pass

@six.add_metaclass(abc.ABCMeta)
class Authz(object):
    name = "Authz"

    def __init__(self,section_config,server):
        """
        Instantiate a new :class:`Authz` object.

        :param section_config: A :class:`dockerauth.config.ConfigSection` object
        :param server: A reference to the :class:`dockerauth.server.auth.AuthServer` object creating this instance.
        """
        self.config = section_config
        self.server = server
        pass

    @abc.abstractmethod
    def authorize(self,request,authn_state=None,**kwargs):
        """
        Authorize a user.
        
        :param request: an instance of :class:`dockerauth.server.http.BaseRequest` or a subclass
        :ptype request: :class:`dockerauth.server.http.BaseRequest`
        :ptype authn_state: an instance of a subclass of :class:`dockerauth.authn.AuthnState` if the authentication class (:class:`dockerauth.authn.Authn`) successfully authenticated the user *and* left state for use in authorization; or None.
        :ptype authn_state: :class:`dockerauth.authn.AuthnState`
        :param kwargs: any extra authentication parameters provided by the request handler
        :ptype kwargs: py:type:`dict`
        :returns: a list of :class:`dockerauth.docker.scope.AuthScope` instances, or None to signal the user doesn't exist in this backend.
        """

    pass
