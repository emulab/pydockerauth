import six
import traceback
import sys
import socket
import struct
from dockerauth.config import (
    global_config_section, ConfigSection, ConfigError )
from dockerauth.log import LOG
from dockerauth.authz import (
    Authz, AuthzError, InternalAuthzError )
from dockerauth.util import isiterable
from dockerauth.util.ip import ( MaskedAddress, IPAddress )
from dockerauth.docker.scope import ( Scope, AuthScope )
from dockerauth.util.auth import ( Cert, CertBundle )

class InvalidAcl(ConfigError):
    pass

class InvalidAclRule(ConfigError):
    pass

class AclRule(object):
    def __init__(self,authz={},user=None,group=None,certname=None,src=None):
        """
        An :class:`AclRule` is the fundamental unit of restriction of
        authorization to a specific user, group, certificate, or source
        IP.  If those items match against an authenticating/authorizing
        client, the client will be authorized to use the
        :class:`dockerauth.docker.scope.AuthScope` instance this rule
        provides.

        :param authz: a ``dict`` authorizing specific :class:`dockerauth.docker.scope.Scope` objects, possibly coupled to token expiration (the ``exp`` key, an ``int`` in seconds); whether or not the token should be multiuse (the ``multiuser`` key, True or False).  Either this dict has the ``scopes`` key (a list of lists, with each member representing a :class:`dockerauth.docker.scope.Scope`); or it has a ``scope`` key (a single list representing a :class:`dockerauth.docker.scope.Scope`).
        :param user: a username to match
        :param group: a group to match
        :param certname: the name of a certificate to match
        :param src: a DNS hostname or IPv4/v6 address to match
        """
        self.authscope = None
        self.user = user
        self.group = group
        self.certname = certname
        self.src = None
        if src is not None:
            self.src = MaskedAddress.parse(src)
        if not authz:
            raise InvalidAcl("no authz with scope/scopes")
        self.authz = authz
        if "scopes" in authz and "scope" in authz:
            raise InvalidAcl("cannot supply both scopes and scope"
                             " in one acl authz dict: '%s'" % (str(authz)))
        if "exp" in authz:
            _exp = int(authz["exp"])
        else:
            _exp = None
        if "multiuse" in authz:
            _mu = bool(authz["multiuse"])
        else:
            _mu = False
        if "scopes" in authz:
            _sl = authz["scopes"]
        elif "scope" in authz:
            _sl = [ authz["scope"] ]
        else:
            raise InvalidAcl("no scope specified in authz: '%s'"
                             % (str(authz)))
        _sl = [Scope(*x) for x in _sl]
        self.authscope = AuthScope(list(_sl),exp=_exp,multiuse=_mu)
        pass

    @classmethod
    def make(kls,scopes,src=None,exp=None):
        if type(scopes) == list and type(scopes[0]) == list:
            return AclRule(src=src,authz={"scopes":scopes,"exp":exp})
        else:
            return AclRule(src=src,authz={"scope":scopes,"exp":exp})
        pass

    def match(self,user,groups,certnames,src):
        LOG.debug("trying to match: (%s,%s,%s,%s) to %s"
                  % (user,groups,str(certnames),src,self))
        if self.user != None and self.user != user:
            return False
        if self.group != None and self.group not in groups:
            return False
        if self.certname != None and self.certname not in certnames:
            return False
        if src is None and self.src is not None:
            return False
        elif self.src is None or src is None:
            LOG.debug("matched: (%s,%s,%s) to %s" % (user,groups,src,self))
            return True
        elif src is not None:
            if src.bytelen() != self.src.addr.bytelen():
                LOG.debug("len(src) %d != len(self.src) %d"
                          % (src.bytelen(),self.src.addr.bytelen()))
                return False
            elif src.int & self.src.mask.int \
              != self.src.addr.int & self.src.mask.int:
                LOG.debug("srcaddr %x != self.srcaddr %x & self.srcmask %x"
                          % (src.int,self.src.addr.int,self.src.mask.int))
                return False
        LOG.debug("matched: (%s,%s,%s) to %s" % (user,groups,src,self))
        return True

    def __repr__(self):
        ss = ""
        if self.user is not None:
            if len(ss):
                ss += ","
            ss += "user=%s" % (self.user)
        if self.group is not None:
            if len(ss):
                ss += ","
            ss += "group=%s" % (self.group)
        if self.src is not None:
            if len(ss):
                ss += ","
            ss += "src=%r" % (repr(self.src))
        if self.certname is not None:
            if len(ss):
                ss += ","
            ss += "certname=%s" % (self.certname)
        return "AclRule(%s => %s)" % (ss,self.authscope)

    pass

class StaticAclAuth(Authz):
    name = "StaticAclAuth"

    def __init__(self,section_config,server):
        Authz.__init__(self,section_config,server)
        if not isinstance(section_config,StaticAclConfig):
            raise ConfigError("expecting StaticAclConfig config section")
        self.groups = section_config.groups
        self.acls = section_config.acls
        self.users = {}
        self.certsbyname = section_config.certsbyname
        self.certnamesbyfingerprint = section_config.certnamesbyfingerprint
        for g in self.groups:
            for u in self.groups[g]:
                if not u in self.users:
                    self.users[u] = []
                self.users[u].append(g)
            pass
        pass

    def authorize(self,request,**kwargs):
        matching = []
        groups = []
        if request.username in self.users:
            groups = self.users[request.username]
        for a in self.acls:
            src = None
            if request.client_ip:
                if not isinstance(request.client_ip,IPAddress):
                    src = IPAddress.parse(request.client_ip)
                else:
                    src = request.client_ip
            certnames = []
            if request.client_cert and request.client_cert_verified:
                cert = Cert(request.client_cert)
                if cert.fingerprint in self.certnamesbyfingerprint:
                    certnames = self.certnamesbyfingerprint[cert.fingerprint]
            if a.match(request.username,groups,certnames,src):
                matching.append(a.authscope)
            pass
        if len(matching) > 0:
            LOG.debug("user '%s' authorized for: %s"
                      % (request.username,str(matching)))
        ascope = AuthScope.join(matching)
        return ascope

    def __repr__(self):
        return "StaticAclAuth(numgroups=%d,numusers=%d,numcertnames=%d,numacls=%d)" \
            % (len(list(self.groups.keys())),len(self.users),
               len(self.certsbyname),len(self.acls))

    pass

@global_config_section
class StaticAclConfig(ConfigSection):
    name = "acl"
    reconfig = True
    authzclass = StaticAclAuth

    def __init__(self,configdir,jsonblob={}):
        super(StaticAclConfig,self).__init__(configdir,jsonblob)
        self.groups = {}
        self.acls = []
        self.certsbyname = {}
        self.certnamesbyfingerprint = {}
        if type(jsonblob) != dict:
            raise ConfigError("'acl' config item must be a dict")
        for (k,v) in six.iteritems(jsonblob):
            if k not in ['groups','acls','certs']:
                raise ConfigError("invalid acl section key '%s'" % (k))
        if "groups" in jsonblob:
            for (g,v) in six.iteritems(jsonblob["groups"]):
                if not isiterable(v):
                    raise ConfigError("'%s' in groups must be a list" % (g))
                if g in self.groups:
                    raise ConfigError("duplicate group '%s'" % (g))
                self.groups[g] = v
        if "acls" in jsonblob:
            alist = jsonblob["acls"]
            if type(alist) != list:
                raise ConfigError("acls must be a list of dicts")
            for acl in alist:
                if type(acl) != dict:
                    raise ConfigError("acls item must be a dict")
                self.acls.append(AclRule(**acl))
        if "certs" in jsonblob:
            cd = jsonblob["certs"]
            if type(cd) != dict:
                raise ConfigError("certs must be a dict of name to filename")
            for (k,v) in six.iteritems(jsonblob["certs"]):
                if k in self.certsbyname:
                    raise ConfigError("duplicate certfile name '%s'" % (k))
                if not v:
                    raise ConfigError("bad certfile path '%s'" % (v))
                if v[0] != '/':
                    v = self.configdir + "/" + v
                x = CertBundle.bundle_or_cert_fromfile(v)
                self.certsbyname[k] = x
                if isinstance(x,CertBundle):
                    for cert in x:
                        cfp = cert.fingerprint
                        if cfp not in self.certnamesbyfingerprint:
                            self.certnamesbyfingerprint[cfp] = [ k ]
                        else:
                            self.certnamesbyfingerprint[cfp].append(k)
                elif isinstance(x,Cert):
                    cfp = x.fingerprint
                    if cfp not in self.certnamesbyfingerprint:
                        self.certnamesbyfingerprint[cfp] = [ k ]
                    else:
                        self.certnamesbyfingerprint[cfp].append(k)
                else:
                    LOG.warning("failed to load cert %r from %r (%r)",k,v,x)
            pass
        pass

    def __repr__(self):
        return "StaticAclConfig(numgroups=%d,numcerts=%d,numcertnames=%d,numacls=%d)" \
            % (len(list(self.groups.keys())),len(self.certnamesbyfingerprint),
               len(self.certsbyname),len(self.acls))

    def dump(self,full=False,indent=0):
        def __newline(extra=0):
            return "\n" + " " * (indent+2+extra)
        retval = repr(self) + "(" + __newline() + "groups=["
        for (k,v) in six.iteritems(self.groups):
            retval += __newline(2) + k + "=" + str(v)
        retval += "]," + __newline() + "certs={"
        for (name,x) in six.iteritems(self.certsbyname):
            retval += __newline(2) + name + "=" + repr(x) + ","
        retval += "}," + __newline() + "acls=["
        for acl in self.acls:
            retval += __newline(2) + "  " + repr(acl)
        retval += "])"
        return retval
    pass
