#!/bin/env python

import os, os.path
import sys
from flask import Flask,request,make_response
import dockerauth.log
from dockerauth.log import LOG
from dockerauth.config import ( find_configfile,add_default_configfile )
from dockerauth.server.auth import AuthServer
from dockerauth.server.http import DockerRegistryRequest

#
# Check if we are on an Emulab boss.
#
TB = "@prefix@"
if TB != "" and os.path.exists(TB):
    add_default_configfile("%s/etc/dockerauth/config.json" % (TB,))

#
# Initialize our Flask app immediately, so we can log during config
# processing.  Use "application" so this can trivially be run from an
# WSGI environment.
#
application = Flask('dockerauth')
dockerauth.log.setLogger(application.logger)

server = None

def convert_flask_request(request):
    """
    Convert a Flask :class:`flask.Request` object to a
    :class:`dockerauth.server.http.DockerRegistryRequest` object.
    
    :param request: a Flask :class:`flask.Request` object.
    :ptype request: :class:`flask.Request`
    :returns: a :class:`dockerauth.server.http.DockerRegistryRequest` object.
    """
    return DockerRegistryRequest(
        request.method,request.url,request.remote_addr,
        args=dict(request.args),headers=request.headers,
        environ=dict(request.environ),framework_request_obj=request)

#@application.route("/service/docker/token/v2")
def docker_token_v2_handler():
    response = server.handle_request(convert_flask_request(request))
    return make_response(response.content,response.code,response.headers)

def main():
    """
    The Flask standalone entrypoint.
    """
    
    global server
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-f","--config-file",default=find_configfile())
    args = parser.parse_args()

    server = AuthServer(args.config_file,standalone=True)
    LOG.info("created standalone AuthServer")

    # Use an explicit registration instead of an @application.route
    # decorator so we can allow the endpoint URL to be configured.
    application.add_url_rule(server.config.server.endpoint_url,
                             'docker_token_v2_handler',docker_token_v2_handler)

    if not server.config.tls.server_privkey_file \
       or not server.config.tls.server_cert_file:
       LOG.error("tls.{server_privkey,server_cert} required in standalone mode!")
       sys.exit(1)

    server.drop_privileges()
    if server.config.server.threaded:
        server.spawn_reconfigure_thread()
    else:
        application.before_request(server.check_and_reconfigure_once)
    (host,port) = (server.config.server.host,server.config.server.port)
    LOG.info("starting standalone on host %s port %s" % (host,str(port)))
    application.run(host=host,port=port,threaded=server.config.server.threaded,
                    ssl_context=(server.config.tls.server_cert_file,
                                 server.config.tls.server_privkey_file))
    server.interrupt_reconfigure_thread()
    sys.exit(1)

def wsgi():
    """
    The Flask WSGI entrypoint (really, anything where the Flask server
    is not running in standalone mode, i.e. is not the HTTP server).

    :returns: the application object.
    """
    global server
    server = AuthServer(find_configfile())

    # Use an explicit registration instead of an @application.route
    # decorator so we can allow the endpoint URL to be configured.
    application.add_url_rule(server.config.server.endpoint_url,
                             'docker_token_v2_handler',docker_token_v2_handler)

    LOG.info("created WSGI AuthServer")

    server.drop_privileges()
    if server.config.server.threaded:
        server.spawn_reconfigure_thread()
    else:
        application.before_request(server.check_and_reconfigure_once)

    #
    # Return the application to our caller, who must set
    # __main__.application so that things like mod_wsgi work.
    #
    return application
