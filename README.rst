``pydockerauth``
================

.. image:: https://gitlab.flux.utah.edu/emulab/pydockerauth/badges/master/build.svg
    :target: https://gitlab.flux.utah.edu/emulab/pydockerauth
    :alt: Build Status

.. image:: https://gitlab.flux.utah.edu/emulab/dockerauth/badges/master/test.svg
    :target: https://gitlab.flux.utah.edu/emulab/dockerauth
    :alt: Test Status

``pydockerauth`` (https://gitlab.flux.utah.edu/emulab/pydockerauth) is a
simple Docker Registry token authentication/authorization server written
in Python.  Its modular design supports multiple web server frameworks,
like Flask and Twisted.

When a Docker registry (https://docs.docker.com/registry/) is
appropriately configured to point at a token auth server like a
``pydockerauth`` server, the registry will redirect the client to the
``pydockerauth`` server via URL to authenticate using HTTP basic auth.
If the server authenticates and authorizes the user, it generates a JWT
token (https://docs.docker.com/registry/spec/auth/token/) that
authorizes the user to a subset of the requested authorization scopes.
In the Docker Registry API, a scope authorizes a user to pull image
layers from a repository; to list the catalog of repositories, etc.

The ``pydockerauth`` server can be configured to run from a web server
via WSGI, or can be run standalone, if the selected web framework
permits.  For any serious application, you should run it via WSGI;
however, the standalone version is multithread-safe.

You can trivially add your own .. class::dockerauth.authn.Authn() (authentication) and Authz (authorization)
modules.

``pydockerauth`` supports Python 2 and 3 on Linux and FreeBSD; these are
the only tested platforms.  The various Authn/Authz backends may require
additional Python modules beyond the base set.

(This server is conceptually similar to the golang-based
https://github.com/cesanta/docker_auth, but a Python implementation was
desirable for us.  If you want golang, go there.)
